import scala.collection.mutable.ListBuffer

/** A Bus (PublicTransportVehicle).
 * 
 *  @constructor create a Bus (PublicTransportVehicle) 
 */

class Bus(_id: String, _speed:Int, _stops: ListBuffer[String], _lineID:String, _atTheEnd:String) extends PublicTransportVehicle(_id,"bus", _speed,_stops, 10, _lineID,_atTheEnd) {  
  
  
/** Creates a string representation of the Bus.
  * 
  *  @return the string summary
  */
  override def toString: String = {
      "id: "+id+"  - pos: "+actualPositionVect
    }
  }