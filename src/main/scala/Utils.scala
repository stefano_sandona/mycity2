import akka.actor.{Props, Actor, ActorSystem, ActorRef};
import org.json._
import scala.collection.mutable.Queue;
import scala.collection.mutable.Map;
import scala.collection.mutable.ListBuffer



object Utils {  
  
  
  case class AcceptMsg(senderID:String, me:MobileEntity) extends Junction.Command
    
  case class AcceptedMsg(meID:String, way:String) extends Junction.Command
  
  case class EnterMsg(senderID:String, me:MobileEntity)
  
  case class EnterFromStopMsg(senderID:String, me:MobileEntity)
  case class EnterFromServiceMsg(senderID:String, me:MobileEntity)
        
  case class EventMsg(frameID:Long,json:JSONObject)
    
  case class FrameSentMsg(id:Long) extends Junction.Command
  
  case class ChildFrameSentMsg(id:Long)
  case class FrameMsg(id:Long)
  
  case class StopAckMsg() extends Junction.Command
  
  
  

  val unit=1
  
  val transport_size = collection.immutable.HashMap("feet" -> 2, "bike" -> 4, "car" -> 5, "bus" -> 8, "tram" -> 16)
  
  val transport_speed = collection.immutable.HashMap("feet" -> Array(1,2), "bike" -> Array(2,5), "car" -> Array(10,20), "bus" -> Array(5), "tram" -> Array(10))

  val statusType = collection.immutable.HashMap("sleeping" -> 0, 
                                                "waiting" -> 1,
                                                "traveling"-> 2,
                                                "ready" ->3,
                                                "serving" -> 4)
  val transportWaysType = Vector("tramlane","street","bikelane","paviment")
  
  
  val line1:ListBuffer[String]=ListBuffer[String]()
  val line2:ListBuffer[String]=ListBuffer[String]()
  val line3:ListBuffer[String]=ListBuffer[String]()
  
  val lines:ListBuffer[ListBuffer[String]]=ListBuffer(line1, line2,line3)
  
  //Map[ServiceName, ListBuffer[RoutesNumber]]
  val servicesRoutesTable:Map[String,ListBuffer[Int]]=Map[String,ListBuffer[Int]]()
  
  //Map[RoutesNumber, ListBuffer[StopName]]
  val routesStopTable:Map[Int,ListBuffer[String]]=Map[Int,ListBuffer[String]]()

  
  trait ReaperWatched { this: Actor =>
  override def preStart() {
    context.actorSelection("/user/" + Reaper.name) ! Reaper.WatchMe(self)
    context.actorSelection("/user/" + EventHandler.name) ! EventHandler.Subscribe()
  }
  }

  
  
  
}