import akka.actor.{ActorLogging, ActorRef,ActorSelection,RootActorPath}
import akka.persistence.{SnapshotOffer, PersistentActor}
import org.json._
import akka.persistence.Recovery
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import java.io._
import akka.cluster.Cluster
import scala.concurrent.duration._
import akka.cluster.ClusterEvent._


object CentralEventHandler {
  trait Command
  case class Push(frameID:Long,jsonArray:String) extends Command
  case class StopMsg() extends Command
  case class Subscribe() extends Command

  trait Event
  case class Push_Evt(frameID:Long,jsonArray:String) extends Event
  
  val name="ceh" //Actor name
  
  val last_frame=1000  //last accepted frame ID
}


/** A Centra Event Handler.
 * 
 *  @constructor create the Central Event Handler (Singleton)
 */
class CentralEventHandler extends PersistentActor with ActorLogging {
  import CentralEventHandler._
  
  val actorName=self.path.name
  
  var termination=false  //if the actor is waiting termination 
  
  log.info("Singleton created")
  
  override def preStart() {
    //subscribe to Cluster Events
    (Cluster(context.system)).subscribe(self,classOf[MemberUp])
  }
  
  var frontend:ActorSelection=null
  
  //prevent the recovery from the journal
  override def recovery = Recovery.none

  //persistence ID
  override def persistenceId: String = self.path.parent.name + "-" + self.path.name

  //Collected events
  //Map[frameID, events]
  val events:Map[Long,JSONArray]=Map[Long,JSONArray]()
  
  //Senders per frame
  //Map[frameID,senders]
  val receivedEvents:Map[Long,ListBuffer[String]]=Map[Long,ListBuffer[String]]()
  
  var actualJObject:JSONObject=new JSONObject()
  
  //entities that will publish events
  val publishingEntities:ListBuffer[String]=ListBuffer[String]()
  
  /*Delete file if already present*/
  
  val f = new File("city.json");
         
  var b = f.createNewFile();
                  
  f.delete();
         
  b = f.createNewFile(); 
  
  /*End Delete file if already present*/
  
  
  val fileW = new FileWriter("city.json")
  val writer = new BufferedWriter(fileW)
  var first:Boolean=true
  writer.append("[")
  writer.newLine()
  
  /*End file initialization*/
  
   /** Add a JSON array to another
    * 
    *  @param the first JSON array
    *  @param the second json array
    */ 
  def concatArray(arr1:JSONArray, arr2:JSONArray){
    for(i<- 0 to arr2.length()-1){
      arr1.put(arr2.get(i));
    }
}
  
  
  def updateState(event: Push_Evt): Unit ={
    val ja:JSONArray=new JSONArray(event.jsonArray)
      if(!(receivedEvents contains event.frameID)){
          receivedEvents(event.frameID)= ListBuffer[String]()
      }
      if(!(events contains event.frameID)){
          events(event.frameID)= new JSONArray()
        }
      if(ja.length!=0){
        concatArray(events(event.frameID),ja)
      }
      
      receivedEvents(event.frameID)+=""+sender
      
      if(receivedEvents(event.frameID).length==publishingEntities.length){
          log.info(s"FRAME ${event.frameID} OK")
          actualJObject.put("id", event.frameID)
          actualJObject.put("events",events(event.frameID))
          if(first){
            writer.append(""+actualJObject)
          }
          else{
            writer.append(",")
            writer.newLine()
            writer.append(""+actualJObject)
          }
          first=false
          actualJObject=new JSONObject()
          
          if(event.frameID==last_frame){
            log.info("last frame detected")
            frontend ! Frontend.StopMsg()
            termination=true
          }
          
      }
  }


  val receiveRecover: Receive = {
    case evt: Push_Evt =>
      updateState(evt)
  }

  val receiveCommand: Receive = {
    /*Message received from an EventHandler to publish the collected events for a particular frame*/
    case Push(frameID,json) if(!termination) =>
      persist(Push_Evt(frameID,json)) { evt =>
        updateState(evt)
      }
     
    /*Message received from an EventHandler to publish the collected events for a particular frame
     * but the actor is waiting termination*/
    case Push(frameID,json) if(termination) =>
      //log.info("terminating => throw away message")
      
    /*Message received from the frontend to stop the system (store the file)*/
    case StopMsg() =>
      writer.append("]")
      writer.close();
      log.info("Singleton STOP msg") 
      Cluster(context.system).registerOnMemberRemoved(self ! "member-removed")
      Cluster(context.system).leave(Cluster(context.system).selfAddress)
      
    case "member-removed" ⇒
      // Let singletons hand over gracefully before stopping the system
      import context.dispatcher
      log.info("member-removed")
      (context.system).scheduler.scheduleOnce(10.seconds, self, "stop-system")
 
    case "stop-system" ⇒
      log.info("stop-system")
      (context.system).terminate()
      
    /*Message received from an EventHandler to subscribe to the publishing entities*/
    case Subscribe() =>
        log.info(s"central subscribe from ${sender}")
        publishingEntities+=(""+sender)
        
    /*Message received from the cluster to indicate a node is UP*/
    case MemberUp(member) =>
      if(member.hasRole("frontend")){
           log.info("frontend detected")
           frontend=context.actorSelection(RootActorPath(member.address) / "user" / Frontend.name)
      }
      
    case default =>
       log.info(s"Received an Unhandled message from ${sender} -> ${default}")
  }
}