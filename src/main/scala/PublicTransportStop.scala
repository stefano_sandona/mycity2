import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection};
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.collection.mutable.Queue
import org.json._


class PublicTransportStop(junctionEntry:ActorRef, stopType:String, ptServices:ListBuffer[String]) extends Actor with ActorLogging with Utils.ReaperWatched{
  import PublicTransportStop._
  val ID=self.path.name
  
  var pt_vehicles:ListBuffer[PublicTransportVehicle]=ListBuffer[PublicTransportVehicle]()
  
  var frameWaiting:Map[Long,Int]=Map[Long,Int]()
  var frame:Long=0
  var waitingMode:Boolean=false
  var alreadySentEvent:Boolean=false
  
  //Current customers
  var customers:ListBuffer[MobileEntity]=ListBuffer[MobileEntity]()
  
  var eventHandler:ActorSelection=context.actorSelection("/user/eventHandler");
 
  val waitingQueues:Map[String,Queue[Person]]=Map[String,Queue[Person]]()
  
  //Map[wayType,MobileEntity] -> people that are sent to the link (ack waiting)
  val waysServing:Map[String,MobileEntity]=Map("tramlane" -> null,
                             "street" -> null,
                             "bikelane" -> null,
                             "paviment" -> null)

    
   val waysReady:Map[String,Queue[MobileEntity]]=Map("tramlane" -> Queue[MobileEntity](),
                             "street" -> Queue[MobileEntity](),
                             "bikelane" -> Queue[MobileEntity](),
                             "paviment" -> Queue[MobileEntity]())
   
   

    def summary():JSONObject={
      val json:JSONObject=new JSONObject()
      var entities:JSONArray=new JSONArray()
      for(c <- customers){
          val e:JSONObject=c.toJson()
          if(c==waysServing(c.way)){
            e.put("serving",true)
          }
          else{
            e.put("serving",false)
          }
          entities.put(e)
      }

      json.put("id",ID)
      json.put("type","stop")
      json.put("entities",entities)
      return json
    }
                             
                             
  def checkWaysReady(){
      for(way <- waysReady){
          if(waysServing(way._1) == null && !(way._2.isEmpty)){
            waysServing(way._1)=(way._2).dequeue()
            junctionEntry ! Utils.EnterFromStopMsg(ID,waysServing(way._1))
          }
      }  
  }
  
  def welcome(mobileEntity:MobileEntity){
    if(mobileEntity.isInstanceOf[PublicTransportVehicle]){ //tram or bus
        val peopleExiting:ListBuffer[Person] =mobileEntity.asInstanceOf[PublicTransportVehicle].unload(ptServices)
        val peopleEntering:ListBuffer[Person] = ListBuffer[Person]()
        var availability=mobileEntity.asInstanceOf[PublicTransportVehicle].availability
        val lineID=mobileEntity.asInstanceOf[PublicTransportVehicle].lineID
        while(availability>0 && (waitingQueues contains lineID) && !(waitingQueues(lineID).isEmpty)){
          val person=(waitingQueues(lineID).dequeue())
          peopleEntering+=person
          customers-=person
          availability-=1
        }
        mobileEntity.asInstanceOf[PublicTransportVehicle].load(peopleEntering)
        waysReady(mobileEntity.way).enqueue(mobileEntity) //enqueue the tram/bus
        for(p <- peopleExiting){
          p.alreadyTook=true  //the person has already took the public transport vehicle => from now he/she moves by feet
          waysReady(p.way).enqueue(p)
        }
        checkWaysReady()
      }
    else{  //person
      val lineID=mobileEntity.asInstanceOf[Person].lineID
      if(!(waitingQueues contains lineID)){
        waitingQueues(lineID)=Queue[Person]()
      }
      waitingQueues(lineID)+=mobileEntity.asInstanceOf[Person]
      }
  }
                             
  
  def receive = start
  
  def end: Receive = {       
    case _  =>
      //log.info(s"throw away message from ${sender}")
  }
  
  def start:Receive = {
    
    case Utils.FrameMsg(id:Long) =>
      if(!(alreadySentEvent)){
        eventHandler ! Utils.EventMsg(frame,summary())
        junctionEntry ! Utils.ChildFrameSentMsg(frame)
        alreadySentEvent=true
      }
      if(!(frameWaiting contains frame)){
        frameWaiting(frame)=1
      }
      if(frameWaiting(frame)!=0){
        waitingMode=true
      }
      if(frameWaiting(frame)==0 && alreadySentEvent){
        waitingMode=false
        frameWaiting-=frame
        frame+=1
        alreadySentEvent=false
        if(!(frameWaiting contains frame)){
          frameWaiting(frame)=1
        }
      }
      self ! Utils.FrameMsg(frame)
      
      
    case Utils.FrameSentMsg(id:Long) =>
        if(!(frameWaiting contains id)){
          frameWaiting(id)=1
        }
        else{
          frameWaiting(id)-=1
        }
    
    case InitMsg(ptv) =>
        pt_vehicles=ptv
        
      /*to start, every customer in the system is sent outside*/
    case StartMsg() => 
        log.info(s"StartMSg STOP")
        self ! Utils.FrameMsg(frame)
        while(!pt_vehicles.isEmpty){
          welcome(pt_vehicles.remove(0))
        }
        
    case StopMsg()=>
        sender() ! Utils.StopAckMsg()
        log.info("StopMsg STOP")
        context.become(end)
        
    
    case msg:Any if waitingMode =>
      self forward msg 
      
    
    case Utils.AcceptMsg(senderID,mobileEntity) =>
      sender() ! Utils.AcceptedMsg(mobileEntity.id,mobileEntity.way)
    
    case Utils.EnterMsg(senderID, mobileEntity)=>
      sender ! Utils.AcceptedMsg(mobileEntity.id, mobileEntity.way)
      customers+=mobileEntity
      welcome(mobileEntity)
    
    case Utils.AcceptedMsg(idx, way) =>
          val mobileEntity = waysServing(way)
          customers-=mobileEntity
          waysServing(way)=null
          //check if we can accept someone
          checkWaysReady()
     
      
  }
  
  
                             
                   
}


object PublicTransportStop{
  case class InitMsg(pt_vehicles:ListBuffer[PublicTransportVehicle])
  case class StartMsg()
  case class StopMsg()
}