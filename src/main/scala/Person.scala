import org.json._

/** A Person (Mobile Entity).
 * 
 *  @constructor create a new person (mobile entity) with a source and a publicTransport value
 *  @param publicTransport indicates if the person uses the public transports or not
 *  @param source the actual destination of the mobile entity
 */

class Person(_id: String, _transportType: String, _speed:Int, var source:String, _dest: String, val publicTransport:Boolean) extends MobileEntity(_id,_transportType, _speed, _dest) {   
    
  //indicate id the Person already took a public transport vehicle for the actual round trip
  var alreadyTook=false
  
  //indicate the next lineID to take 
  var lineID:String=null
  
/** Creates a string representation of the Person.
  * 
  *  @return the string summary
  */
  override def toString: String = {
      "id: "+id+"  - pos: "+actualPositionVect
  }
    
/** Called when a Person reached its end to clean its status and reverse source and destination
  */
  def arrived(){
      val aux=source
      source=dest
      dest=aux
      alreadyTook=false
  }
    
/** Creates a JSON object that represents the Person
  * 
  *  @return the JSON object summary
  */
  override def toJson():JSONObject = {
    val json:JSONObject=new JSONObject()
    json.put("id",id)
    json.put("pos",actualPositionVect.toJson())
    json.put("transport",_transportType)
    return json
  }
    
    
    
  }