import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection};
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map;
import scala.collection.mutable.Queue;
import org.json._


/** A Service.
 * 
 *  @constructor create a new Service
 *  @param master Junction (exit)
 */

class Service(junctionEntry:ActorRef) extends Actor with ActorLogging with Utils.ReaperWatched{
  import Service._
    
  //service ID
  val ID=self.path.name
  
  //People permanence time 
  var permanenceTime: Int=20
  
  //Current customers
  var customers:ListBuffer[Person]=ListBuffer[Person]()
  
  //Number of waiting acks to stop
  var waitingAcks:Int=0
  
  /*Distributed frame correlation algorithm variables*/
  
  //frameID
  var frame:Long=0
  
  //Number of waiting acks to proceed the computation from the master Junction
  var frameWaiting:Map[Long,Int]=Map[Long,Int]()
  
  
  //indicate if the computation can proceed or not
  var waitingMode:Boolean=false
  
  //indicate if the actual frame situation was already sent to the EventHandler
  var alreadySentEvent:Boolean=false
  
  //indicate if an Ack was already sent to the Parent Junction Actor
  var alreadySentParent:Boolean=false
  
  /*End Distributed frame correlation algorithm variables*/

  
  //Local Event Handler
  var eventHandler:ActorSelection=context.actorSelection("/user/eventHandler");
    
  //Serving entity per waytype
  val waysServing:Map[String,MobileEntity]=Map("tramLane" -> null,
                             "street" -> null,
                             "bikelane" -> null,
                             "paviment" -> null)
    
  //ready entities per waytype
  val waysReady:Map[String,Queue[MobileEntity]]=Map("tramLane" -> Queue[MobileEntity](),
                             "street" -> Queue[MobileEntity](),
                             "bikelane" -> Queue[MobileEntity](),
                             "paviment" -> Queue[MobileEntity]())
  

   /** Creates a JSON object that contains a summary of the owned mobile entities.
    * 
    *  @return the summary JSON object
    */ 
  def summary():JSONObject={
      val json:JSONObject=new JSONObject()
      var pos=0
      var entities:JSONArray=new JSONArray()
      for(c <- customers){
          val e:JSONObject=c.toJson()
          if(c==waysServing(c.way)){
            e.put("serving",true)
          }
          else{
            e.put("serving",false)
          }
          entities.put(e)
      }
      json.put("id",ID)
      json.put("type","service")
      json.put("entities",entities)
      return json
    }
  
    def checkWaysReady(){
      for(way <- waysReady){
          if(waysServing(way._1) == null && !(way._2.isEmpty)){
            waysServing(way._1)=(way._2).dequeue()
            junctionEntry ! Utils.EnterFromServiceMsg(ID,waysServing(way._1))
          }
      }  
  }
  
  
  
  def receive = start
  
  def end: Receive = {       
    case _  =>
      //log.info(s"throw away message from ${sender}")
  }
  
  def start:Receive = {     
    case Utils.FrameMsg(id:Long) =>
      if(!(alreadySentEvent)){
        eventHandler ! Utils.EventMsg(frame,summary())
        junctionEntry ! Utils.ChildFrameSentMsg(frame)
        alreadySentEvent=true
      }
      if(!(frameWaiting contains frame)){
        frameWaiting(frame)=1
      }
      if(frameWaiting(frame)!=0){
        waitingMode=true
      }
      if(frameWaiting(frame)==0 && alreadySentEvent){
        waitingMode=false
        frameWaiting-=frame
        frame+=1
        alreadySentEvent=false
        if(!(frameWaiting contains frame)){
          frameWaiting(frame)=1
        }
      }
      self ! Utils.FrameMsg(frame)
      
      
    case Utils.FrameSentMsg(id:Long) =>
        if(!(frameWaiting contains id)){
          frameWaiting(id)=1
        }
        else{
          frameWaiting(id)-=1
        }
    
    
    
    /*Message received from the master junction as initialization*/
    case InitMsg(cust, permTime) =>
        permanenceTime=permTime
        customers=cust
        
      /*Message received from the master junction to start*/
      case StartMsg() => 
        log.info("StartMsg SERVICE")
        self ! Utils.FrameMsg(frame)
        while(!customers.isEmpty){
          self ! SleepMsg(customers.remove(0),0)
        }
        
        
      /*Message received from the master Junction to stop the Service and its children */
      case StopMsg()=>
        junctionEntry ! Utils.StopAckMsg()
        log.info("StopMsg SERVICE")
        context.become(end)
        
      /*Each Message receivedduring waiting mode is requeued */ 
      case msg:Any if waitingMode =>
        self forward msg 
        
      /*Message received from the master Junction to accept a mobile entity */
      case Utils.AcceptMsg(senderID,mobileEntity) =>
          sender() ! Utils.AcceptedMsg(mobileEntity.id,mobileEntity.way)
    
      
      /*Message received from a link to accept a mobile entity */
      case Utils.EnterMsg(senderID,mobileEntity) =>
        sender ! Utils.AcceptedMsg(mobileEntity.id, mobileEntity.way)
        mobileEntity.status=Utils.statusType("sleeping")
        if(mobileEntity.isInstanceOf[Person]){
          mobileEntity.asInstanceOf[Person].arrived()
        }
        self ! SleepMsg(mobileEntity,permanenceTime)
        customers+=mobileEntity.asInstanceOf[Person]
        
        
      /*Message received from itself to move the mobile entity */
      case SleepMsg(mobileEntity,actualPermanenceTime) =>
        /*if the permanence time is end*/
        if(actualPermanenceTime==0){
          waysReady(mobileEntity.way).enqueue(mobileEntity)
          checkWaysReady()
        }
        else{ //if the entity is not at the end of the link, continue to travel
          self ! SleepMsg(mobileEntity, actualPermanenceTime-1)
        }
       
     /*Ack message to confirm the acceptance of a sent mobile entity */
     case Utils.AcceptedMsg(id, way) =>  
          val mobileEntity = waysServing(way)
          customers-=mobileEntity.asInstanceOf[Person]
          waysServing(way)=null
          //check if we can accept someone
          checkWaysReady()
          
          

    case _ =>
        throw new Exception("["+ID+"] Received a strange message")
  }
}

object Service{
  case class AddLinkIN(linkINWays:Map[String,Vector2], linkways: Map[String,Tuple2[Vector2, Vector2]])  
  case class InitMsg(customers:ListBuffer[Person], permanenceTime:Int)
  case class StartMsg()
  case class StopMsg()
  case class SleepMsg(me:MobileEntity, permanenceTime:Int)
}