import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import akka.actor._
import akka.cluster.Cluster
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings}
import akka.pattern.ask
import akka.persistence.journal.leveldb.{SharedLeveldbStore, SharedLeveldbJournal}
import akka.util.Timeout
import scala.collection.mutable.Map

import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings}

import akka.NotUsed
import akka.actor._

import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Source, Flow }
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.UpgradeToWebSocket
import akka.http.scaladsl.model.ws.{ TextMessage, Message }
import akka.http.scaladsl.model.{ HttpResponse, Uri, HttpRequest }
import akka.http.scaladsl.model.HttpMethods._
import akka.stream.scaladsl.Sink



import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.io.StdIn

import akka.http.scaladsl.model.ws.BinaryMessage


object MyCityApp_N1 extends App {
  import common._
  var eventHandler:ActorRef=null
  var reaper:ActorRef=null
  val port="2553"
  
  val config=ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
    withFallback(ConfigFactory.load().getConfig("Frontend"))

  // Create an Akka system
  val system = ActorSystem("ClusterSystem", config)
  
  startupSharedJournal(system, startStore = (port == "2553"), path =
        ActorPath.fromString("akka.tcp://ClusterSystem@127.0.0.1:2553/user/store"))
        
  ClusterSharding(system).start(
        typeName = Junction.shardName,
        entityProps = Junction.props(),
        settings = ClusterShardingSettings(system),
        extractEntityId = Junction.idExtractor,
        extractShardId = Junction.shardResolver)
        
  /*val master = system.actorOf(ClusterSingletonManager.props(
        singletonProps = Props[CentralEventHandler],
        terminationMessage = PoisonPill,
        settings = ClusterSingletonManagerSettings(system).withRole(None)
      ), name = "master")*/
        
  Cluster(system) registerOnMemberUp {
          println("STARTING FRONTEND")
          val frontend:ActorRef=system.actorOf(Props[Frontend], Frontend.name)         
          //frontend ! Frontend.InitMsg()
  }
  
  reaper = system.actorOf(Props[Reaper], Reaper.name)
  eventHandler=system.actorOf(Props[EventHandler], EventHandler.name)
  
}

object MyCityApp_N2 extends App {
  import common._
  var eventHandler:ActorRef=null
  var reaper:ActorRef=null
  
  val port="2551"
  
  val config=ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
    withFallback(ConfigFactory.load().getConfig("Backend1"))

  // Create an Akka system
  val system = ActorSystem("ClusterSystem", config)
  
  startupSharedJournal(system, startStore = (port == "2553"), path =
        ActorPath.fromString("akka.tcp://ClusterSystem@127.0.0.1:2553/user/store"))
        
  ClusterSharding(system).start(
        typeName = Junction.shardName,
        entityProps = Junction.props(),
        settings = ClusterShardingSettings(system),
        extractEntityId = Junction.idExtractor,
        extractShardId = Junction.shardResolver)
        
   /*val master = system.actorOf(ClusterSingletonManager.props(
        singletonProps = Props[CentralEventHandler],
        terminationMessage = PoisonPill,
        settings = ClusterSingletonManagerSettings(system).withRole(None)
      ), name = "master")*/
        
  reaper = system.actorOf(Props[Reaper], Reaper.name)
  eventHandler=system.actorOf(Props[EventHandler], EventHandler.name)
  
}

object MyCityApp_N3_CentralEventHandler extends App {
  import common._
  
  var centralEventHandler:ActorRef=null
  
  val port="2552"
          
  val config=ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
    withFallback(ConfigFactory.load().getConfig("CentralEventHandler"))

  // Create an Akka system
  val system = ActorSystem("ClusterSystem", config)
  
  startupSharedJournal(system, startStore = (port == "2553"), path =
        ActorPath.fromString("akka.tcp://ClusterSystem@127.0.0.1:2553/user/store"))
        
  ClusterSharding(system).start(
        typeName = Junction.shardName,
        entityProps = Junction.props(),
        settings = ClusterShardingSettings(system),
        extractEntityId = Junction.idExtractor,
        extractShardId = Junction.shardResolver)
  
  centralEventHandler=system.actorOf(Props[CentralEventHandler], CentralEventHandler.name)
  
}

object common{
  def startupSharedJournal(system: ActorSystem, startStore: Boolean, path: ActorPath): Unit = {
      // Start the shared journal one one node (don't crash this SPOF)
      // This will not be needed with a distributed journal
      if (startStore)
        system.actorOf(Props[SharedLeveldbStore], "store")
      // register the shared journal
      import system.dispatcher
      implicit val timeout = Timeout(15.seconds)
      val f = system.actorSelection(path) ? Identify(None)
      f.onSuccess {
        case ActorIdentity(_, Some(ref)) =>
          SharedLeveldbJournal.setStore(ref, system)
        case _ =>
          system.log.error("Shared journal not started at {}", path)
          system.terminate()
      }
      f.onFailure {
        case _ =>
          system.log.error("Lookup of shared journal at {} timed out", path)
          system.terminate()
      }
    }
  
}