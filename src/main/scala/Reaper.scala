import scala.collection.mutable.ArrayBuffer
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated,PoisonPill}
import akka.cluster.sharding.ClusterSharding
import akka.cluster.sharding.ShardRegion
import akka.cluster.Cluster
import scala.concurrent.duration._
import akka.cluster.ClusterEvent._
import akka.cluster._;

object Reaper {
  val name = "reaper"
  // Used by others to register an Actor for watching
  case class WatchMe(ref: ActorRef)
}

class Reaper extends Actor with ActorLogging {
  import Reaper._
  
  var termination=false
    
  //shared region actor
  val junctionRegion: ActorRef = ClusterSharding(context.system).shardRegion(Junction.shardName)
  
  //monitor the shared region actor
  context.watch(junctionRegion)
  
  //subscribe to the Cluster MemberRemoved event
  Cluster(context.system).subscribe(self,classOf[MemberRemoved])
  

  // Keep track of what we're watching
  val watched = ArrayBuffer.empty[ActorRef]

  
   /** The hook that's called when everything's dead.
    */
  def allSoulsReaped() = {
    log.info("allSoulsReaped")
    context.watch(junctionRegion)
    junctionRegion ! ShardRegion.GracefulShutdown
  }

  final def receive = {
    
    /*Message received from the cluster that indicates that a member is removed by the Cluster Leader*/
    case MemberRemoved(member,previous) if (!termination)=>
      log.info(s"MemberRemoved -> ${member} => Start termination")
      termination=true
      self ! "leave"
      
    case MemberRemoved(member,previous) if (termination)=>
      log.info(s"MemberRemoved -> ${member}")
      //log.info(s"Already terminating -> ${member}")
      
    /*Message received from an entity that wants to be monitored*/
    case WatchMe(ref) =>
      context.watch(ref)
      watched += ref
      
    /*Message to notify the termination of a watched entity*/
    case Terminated(ref) =>
      if(ref==junctionRegion){
        log.info("junctionRegion terminated")
        Cluster(context.system).registerOnMemberRemoved(self ! "member-removed")
        Cluster(context.system).leave(Cluster(context.system).selfAddress)
      }
      else{
        watched -= ref
        if (watched.isEmpty) allSoulsReaped()
      }
      
    /*Messages to manage a graceful system shutdown*/
      
    case "leave" ⇒
      for(w<-watched){
        w ! PoisonPill
      }       
 
    case "member-removed" ⇒
      // Let singletons hand over gracefully before stopping the system
      import context.dispatcher
      log.info("member-removed")
      (context.system).scheduler.scheduleOnce(10.seconds, self, "stop-system")
 
    case "stop-system" ⇒
      log.info("stop-system")
      (context.system).terminate()
      
    /*End Messages to manage a graceful system shutdown*/
  }
}