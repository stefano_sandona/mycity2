import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging,ActorSelection};
import scala.collection.mutable.Map;
import scala.collection.mutable.ListBuffer
import scala.math._;
import scala.collection.mutable.Queue;
import org.json._
import scala.util.control.Breaks._


/** A Link.
 * 
 *  @constructor create a new Link
 *  @param start point
 *  @param end point
 *  @param master Junction
 */

class Link(startVect:Vector2, endVect:Vector2, exitActor:ActorRef) extends Actor with ActorLogging with Utils.ReaperWatched{
  import Link._
    
  //Link ID
  val ID=self.path.name
  
  //Local Event Handler
  var eventHandler:ActorSelection=context.actorSelection("/user/eventHandler");
  
  /*Distributed frame correlation algorithm variables*/
  
  //Number of waiting acks to proceed the computation from the master Junction (or master Service)
  var frameWaiting:Map[Long,Int]=Map[Long,Int]()
  
  //frameID
  var frame:Long=0
  
  //indicate if the computation can proceed or not
  var waitingMode:Boolean=false
  
  //indicate if the actual frame situation was already sent to the EventHandler
  var alreadySentEvent:Boolean=false
  
  /*End Distributed frame correlation algorithm variables*/
  
  
    //entity that is waiting to access the link
    var waitingEntity:Tuple2[MobileEntity,ActorRef]=(null,null)
    
    //vector that represents the link
    val linkVector:Vector2=startVect.getDistanceVector(endVect)
    
    //length of the link
    val linkLength =linkVector.getNorm()
    
    //length of the link in meters
    val linkRealLength =linkLength*Utils.unit
                                                        
    //entities that are moving                                              
    val traveling:ListBuffer[MobileEntity]=ListBuffer[MobileEntity]()
    
    //entity that is currently sent to the master Entity (waiting Ack)
    var serving:MobileEntity=null
    
    //entity to ack
    var toAckMobileEntity:ListBuffer[MobileEntity]=ListBuffer[MobileEntity]()
    var toAckSender:ListBuffer[ActorRef]=ListBuffer[ActorRef]()
    
    
    
    //entities that reached the end of their travel (end of the link or enqueued ot exit)
    val ready:Queue[MobileEntity]=Queue[MobileEntity]()
                             
    //link availability 
    var availability=linkLength
    
            
    /** Check if an entity collides with another one
    * 
    *  @param the other entity
    *  @param the actual entity
    *  @param the actual entity next point
    *  @return true if the entities collide, false otherwise
    */ 
    def checkCollision(otherMobileEntity: MobileEntity, actualMobileEntity: MobileEntity, possibleNextPoint:Vector2):Boolean = {
      //compute the total occupation, the sum of the  2 entities occupation
      val minDistance=otherMobileEntity.length/2 + actualMobileEntity.length/2
      //if the distance is less than the totalOccupation, the entities will collide
      if((otherMobileEntity.actualPositionVect).getDistance((possibleNextPoint))<minDistance)
        return true
      else return false
    }
    
    
    /** Compute the closest available point in the path after a given entity
    * 
    *  @param the other entity
    *  @param the actual entity
    *  @return the computed point
    */
    def getFirstAvailablePositionAfter(referenceMobileEntity:MobileEntity, actualMobileEntity: MobileEntity):Vector2 = {
      //compute the total occupation, the sum of the  2 entities occupation
      val minDistance=referenceMobileEntity.length/2 + actualMobileEntity.length/2
      val minDistanceVect = linkVector.getVectOfLenght(minDistance)
      //reverse the minDistanceVector
      minDistanceVect.reverse()
      //nextPoint
      var nextPoint:Vector2=referenceMobileEntity.actualPositionVect.add(minDistanceVect)
      return nextPoint
    }
    
    
    /** Compute the next available point in the path after a given entity
    * 
    *  @param the other entity
    *  @param the actual entity
    *  @return the computed point
    */
    def getNewPosition(previousEntity:MobileEntity, actualEntity:MobileEntity, possibleNextPoint:Vector2):Vector2={
        val firstAvailablePoint=getFirstAvailablePositionAfter(previousEntity, actualEntity)
        if(firstAvailablePoint.getDistance(endVect) < possibleNextPoint.getDistance(actualEntity.actualExitVect)){
          return possibleNextPoint
        }
        else{
          return firstAvailablePoint
        }
    }
    
    

   /** Check if an entity reached the end of its local crossing path with the given movement
    * 
    *  @param start Point of the movement
    *  @param end point of the movement
    *  @param end point of the path
    *  @return true if the entity reached the end of the crossing path, false otherwise
    */ 
    def checkReachedPoint(start:Vector2, end:Vector2, exit:Vector2):Boolean={
      val d1=start.getDistance(exit)
      val d2=start.getDistance(end)
        if(d1<=d2){
          return true
        }
        else{
          return false
        }
    }
    
   /** Check if an entity reached the end of its local crossing path with the given movement
    * 
    *  @param start Point of the movement
    *  @param end point of the movement
    *  @param end point of the path
    *  @return true if the entity reached the end of the crossing path, false otherwise
    */ 
    def getPositionInQueue(mobileEntity:MobileEntity):Int={
      var pos=0
      breakable {
        for(el <- traveling){
          if(el==mobileEntity){
            break  
          }
          pos+=1
        }
      }
      return pos
    }
    
    /*insert the entity in the right position of the traveling queue*/
    def insertInQueue(mobileEntity:MobileEntity):Int={      
      var pos=0
      
      
      //calculate position in the way based on the distance from the end of the link
      breakable {
        for(elem <- traveling){
          if(elem.distanceToEnd>mobileEntity.distanceToEnd){
            break
          }
          pos+=1
        }
      }
            
      
      //insert in the queue in the right position
      if(pos==traveling.size){
        traveling+=mobileEntity
      }
      else{
        traveling.insert(pos,mobileEntity)
      }
      
      
      return pos
    }
    
  /** Creates a JSON object that contains a summary of the owned mobile entities.
    * 
    *  @return the summary JSON object
    */     
     def summary():JSONObject={
      val json:JSONObject=new JSONObject()
      var entities:JSONArray=new JSONArray()
      for(el <- ready){
          val e:JSONObject=el.toJson()
          if(el==serving){
            e.put("serving",true)
          }
          else{
            e.put("serving",false)
          }
          entities.put(e)
      }
      for(el <- traveling){
          val e:JSONObject=el.toJson()
          e.put("serving",false)
          entities.put(e)
      }
      json.put("id",ID)
      json.put("type","link")
      json.put("entities",entities)
      return json
    }
      
   /** Move a mobile entity.
    * 
    *  @param mobile entity
    *  @return true if the entity reached the end of the link and is the first, false otherwise
    */   
    def moveEntity(mobileEntity: MobileEntity):Boolean={     
      var readyToBeServed=false
      
      
      /* calculate the number of time units to complete*/
      val numberOfTimeUnits=linkRealLength/mobileEntity.speed
      
      val actualDirection=mobileEntity.actualPositionVect.getDistanceVector(mobileEntity.actualExitVect)
        
      var progr:Vector2=null
      try{
        /* calculate the progress vector per time unit*/
        progr = actualDirection.getVectOfLenght(linkLength/numberOfTimeUnits)
      }
      catch{
        case e:Exception => 
          progr = actualDirection
      }
      
      
      //compute the standard next point
      var possibleNextPoint = mobileEntity.actualPositionVect.add(progr)
        

      var readyQueueLastEntity:MobileEntity=null
      if(!ready.isEmpty){
        readyQueueLastEntity=ready.last
      }
      
      //if the entity is ready (is waiting on a queue to exit)
      if((mobileEntity.status==Utils.statusType("ready"))){
        
        /*if the entity is not the first we retrieve the previous element and we move it using this 
        as endPoint*/
        if(ready.front != mobileEntity){
          val indexInQueue=ready.indexOf(mobileEntity)
          val previousEntity=ready(indexInQueue-1)
          
          var nextPoint:Vector2=getFirstAvailablePositionAfter(previousEntity, mobileEntity)
      
          if(checkReachedPoint(mobileEntity.actualPositionVect, nextPoint, mobileEntity.actualExitVect)){
            nextPoint=mobileEntity.actualExitVect
          }
          
          mobileEntity.actualPositionVect=nextPoint
        }
        else{ /*it is the first in the ready queue (a place is free in front of it)
          if the mobile entity reached the end of its travel, fix its possibleNextPoint at the stop bound*/
          readyToBeServed=true
          mobileEntity.actualPositionVect=mobileEntity.actualExitVect
        }
      }
      else{
          var end=false

          if(checkReachedPoint(mobileEntity.actualPositionVect,
                            possibleNextPoint, 
                            mobileEntity.actualExitVect)){
            possibleNextPoint=mobileEntity.actualExitVect
            end=true
          }
          
          
          if(readyQueueLastEntity!=null){
            possibleNextPoint= getNewPosition(readyQueueLastEntity,mobileEntity,possibleNextPoint)
          }
          
          val oldPos=getPositionInQueue(mobileEntity)
          
          //remove the entity from the queue
          traveling-=mobileEntity
            
          /*if the mobile entity collide with another one, 
       		* it's possibleNextPoint is moved in the first available position after the 
       		* colliding entity*/
          var ok=false
          while(!ok){
            ok=true
            breakable {
              for(elem <- traveling){ 
                if(checkCollision(elem,mobileEntity,possibleNextPoint)){
                  ok=false
                  possibleNextPoint=getFirstAvailablePositionAfter(elem, mobileEntity)
                  break
                }
              }
            }
          }
          
          
          /*the possibleNextPoint is fixed at this point, the distance from the end 
       		* is computed to determine the position in the queue*/
          

          if(checkReachedPoint(mobileEntity.actualPositionVect, possibleNextPoint, mobileEntity.actualExitVect)){
            possibleNextPoint=mobileEntity.actualExitVect
            mobileEntity.status=Utils.statusType("ready")
          }

          mobileEntity.actualPositionVect=possibleNextPoint

          mobileEntity.distanceToEnd = (mobileEntity.actualExitVect).getDistance(mobileEntity.actualPositionVect)
          
          if(readyQueueLastEntity!=null){
            if(getFirstAvailablePositionAfter(readyQueueLastEntity, mobileEntity) equals mobileEntity.actualPositionVect){
              mobileEntity.status=Utils.statusType("ready")
            }
          }
          else{
            if(mobileEntity.actualPositionVect equals mobileEntity.actualExitVect){
              mobileEntity.status=Utils.statusType("ready")
            }
          }
                  
          /*if the entity became, it is inserted in the ready queue and removed from the 
           * traveling queue*/
          if((mobileEntity.status==Utils.statusType("ready"))){
            ready+=mobileEntity
            traveling-=(mobileEntity)
          }
          else{
            insertInQueue(mobileEntity)
          }
          
          if((mobileEntity.actualPositionVect equals mobileEntity.actualExitVect) && serving==null){
            readyToBeServed=true
          }
          
          
      }
      
      return readyToBeServed
    }
    
    def checkWaitingEntity(way:String){
      /*Get the waiting entity*/
      val (mobileEntity, sender)=waitingEntity
      
      /*establish if the waiting entity can enter the link*/
      var canEnter=false
    
      if(!(mobileEntity==null) && (availability-mobileEntity.length)>=0){
        canEnter=true
        if(!traveling.isEmpty){
          if(checkCollision(traveling.last, mobileEntity, mobileEntity.actualPositionVect)){
            canEnter=false
          }
        }
        else{
          if(!ready.isEmpty){
            if(checkCollision(ready.last, mobileEntity, mobileEntity.actualPositionVect)){
              canEnter=false
            }
          } 
        }
      }
          
      /*check if we can serve someone in the access waiting queue*/
      if(canEnter){        
          mobileEntity.status=Utils.statusType("traveling")
          
          toAckMobileEntity+=mobileEntity
          toAckSender+=sender


          waitingEntity=(null,null)
          
          availability-=mobileEntity.length
          //get the right way
          traveling+=mobileEntity
          self ! TravelMsg(mobileEntity)
      }
      
    }
  
  
  def receive = start
  
  def end: Receive = {       
    case _  =>
      //log.info(s"throw away message from ${sender}")
  }
  
  def start:Receive = {
    
    case Utils.FrameMsg(id:Long) =>
      if(!(alreadySentEvent)){
        eventHandler ! Utils.EventMsg(frame,summary())
        exitActor ! Utils.ChildFrameSentMsg(frame)
        alreadySentEvent=true
      }
      if(!(frameWaiting contains frame)){
        frameWaiting(frame)=1
      }
      if(frameWaiting(frame)!=0){
        waitingMode=true
      }
      if(frameWaiting(frame)==0 && alreadySentEvent){
        waitingMode=false
        frameWaiting-=frame
        frame+=1
        alreadySentEvent=false
        if(!(frameWaiting contains frame)){
          frameWaiting(frame)=1
        }
      }
      self ! Utils.FrameMsg(frame)
      
      
    case Utils.FrameSentMsg(id:Long) =>
        if(!(frameWaiting contains id)){
          frameWaiting(id)=1
        }
        else{
          frameWaiting(id)-=1
        }
        
    case StopMsg()=>
        sender() ! Utils.StopAckMsg()
        log.info("StopMsg LINK")
        context.become(end)
        
    case StartMsg() =>
        log.info(s"StartMsg LINK")
        self ! Utils.FrameMsg(frame)
        
    
    case msg:Any if waitingMode =>
      self forward msg 
  

      case Utils.AcceptMsg(senderID,mobileEntity) =>

        
        /*the first position is the start point of the correct way */
        mobileEntity.actualPositionVect=startVect
        
        mobileEntity.actualEntryVect=mobileEntity.actualPositionVect
        mobileEntity.actualExitVect=endVect
        
        mobileEntity.distanceToEnd = (mobileEntity.actualExitVect).getDistance(mobileEntity.actualPositionVect)

        
        
        mobileEntity.status=Utils.statusType("waiting")
        
        val tup:Tuple2[MobileEntity,ActorRef]=(mobileEntity, sender)
        waitingEntity=tup
                
        //check if leave the entity in the waiting entities or accept it
        checkWaitingEntity(mobileEntity.way)
        
        
      
      case TravelMsg(mobileEntity) => 
        val readyToBeServed:Boolean=moveEntity(mobileEntity: MobileEntity)

        
        var index=toAckMobileEntity.indexOf(mobileEntity)
        if(index != -1 && !(mobileEntity.actualPositionVect.equals(mobileEntity.actualEntryVect))){
          toAckSender(index) ! Utils.AcceptedMsg(mobileEntity.id, mobileEntity.way)
          toAckMobileEntity.remove(index)
          toAckSender.remove(index)
        }
        index=toAckMobileEntity.indexOf(mobileEntity)
        if(index != -1){
          log.info("PROBLEM A")
        }
        
        /*if the entity is ready to be served it is sent to the next Junction*/
        if(readyToBeServed){
          mobileEntity.status=Utils.statusType("serving")
          serving=mobileEntity
          exitActor ! Utils.EnterMsg(ID,mobileEntity)
         
        }
        else{
          self ! TravelMsg(mobileEntity)
        }
        
        //check if we can accept someone
        checkWaitingEntity(mobileEntity.way)

      
      case Utils.AcceptedMsg(idx, way) =>
          val a=ready.dequeue()
          if(!(a.id).equalsIgnoreCase(idx)){
            log.info(s"PROBLEM B")
          }
          
          availability+=serving.length
          serving=null
          
          checkWaitingEntity(way)


                
    case _ =>
        throw new Exception("["+ID+"] Received a strange message")
  }
}

object Link{
  case class HelloMsg()
  case class AcceptMsg(me:MobileEntity)
  case class TravelMsg(me:MobileEntity)
  case class StartMsg()
  case class StopMsg()
}