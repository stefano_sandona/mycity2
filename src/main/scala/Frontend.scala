
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated,ActorSelection,RootActorPath}
import akka.cluster.sharding.ClusterSharding
import akka.cluster.sharding.ShardRegion
import akka.cluster.Cluster
import scala.concurrent.duration._
import scala.util.Random
import org.json._
import scala.collection.mutable.Map
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import org.graphstream.graph.implementations.SingleGraph;
import scala.collection.JavaConverters._
import akka.cluster.ClusterEvent._
import akka.cluster._;

import akka.cluster.singleton.{ClusterSingletonProxySettings, ClusterSingletonProxy}



/** A Frontend Actor.
 * 
 *  @constructor create the Frontend Actot
 */
class Frontend extends Actor with ActorLogging{
  import Frontend._
  import context.dispatcher
  
  val actorName=self.path.name
  
  //Number of waiting acks to start the system
  var waitingAcks:Map[String,Int]=Map[String,Int]()
  
  //Number of waiting acks to stop the system
  var waitingStopAcks:Int=0
  var ready=false
  

  //Linked junctions
  val neighborhoods:ListBuffer[String]=ListBuffer[String]()
  
  //Nodes that joined the cluster
  val clusterNodes:ListBuffer[Member]=ListBuffer[Member]()
  
  //Shared Region Actor
  val junctionRegion: ActorRef = ClusterSharding(context.system).shardRegion(Junction.shardName)
  
  //Routing Tables per way type  
  val routingTablesWayType:Map[String,Map[String,Map[String,Tuple2[String,Double]]]]=
    Map("paviment" -> Map[String,Map[String,Tuple2[String,Double]]](),
        "bikelane" -> Map[String,Map[String,Tuple2[String,Double]]](),
        "street" -> Map[String,Map[String,Tuple2[String,Double]]](),
        "tramlane" -> Map[String,Map[String,Tuple2[String,Double]]]())
        
  /*City entities*/
        
  val links=ListBuffer[String]()
  val junctions=ListBuffer[String]()
  //Map[ID,serviceType]
  val services:ListBuffer[String]=ListBuffer[String]()
  val stops:Map[String,Tuple2[String,ListBuffer[String]]]=Map[String,Tuple2[String,ListBuffer[String]]]()
  val ptLines:Map[String,Tuple2[ListBuffer[String],String]]=Map[String,Tuple2[ListBuffer[String],String]]()
  
  /*End City Entities*/
  
  var routing:Boolean=false
  
  //Proxy Actor to communicate with the CentralEventHandler Singleton
  /*val masterProxy = context.actorOf(ClusterSingletonProxy.props(
    singletonManagerPath = "/user/master",
    settings = ClusterSingletonProxySettings(context.system).withRole(None)
  ), name = "masterProxy")*/
  var centralEventHandler:ActorSelection=null
  

  override def preStart() {
    context.actorSelection("/user/" + Reaper.name) ! Reaper.WatchMe(self)
    //subscribe to Cluster Events
    (Cluster(context.system)).subscribe(self,classOf[MemberUp])
  }
  
  
  /** Send the routing tables to the City Junctions.
   */
  def sendRoutingTables(){
    log.info("sending routing tables")
    routing=true
    for(wayType<-Utils.transportWaysType){
        for(n<-routingTablesWayType(wayType)){
          if(junctions contains n._1){
            junctionRegion ! Junction.WrapperMessage(n._1,Junction.AddRoutingTable(wayType,routingTablesWayType(wayType)(n._1)))
            waitingAcks(n._1)+=1
          }
        }
      } 

  }
  
  
  /** Send the inhabitants (from JSON file) to the City Junctions.
   */
  def sendInhabitants(){
    log.info("sending inhabitants")
    var inhabitants:Map[String,ListBuffer[Person]]=
      Map[String,ListBuffer[Person]]()
      
    var junctionService:Map[String,String]=Map[String,String]()
      
    val url="src/main/resources/inhabitants.json"
      
    val lines = scala.io.Source.fromFile(url, "utf-8").getLines.mkString  
      
    val fileLines:JSONArray=new JSONArray(lines)
    
    for(i <- 0 to fileLines.length()-1){
        val line:JSONObject = fileLines.getJSONObject(i)
        val junctionEntry:String=line.getString("source_junction")
        if(!(inhabitants contains junctionEntry)){
          inhabitants(junctionEntry)=ListBuffer[Person]()
        }
        if(!(junctionService contains junctionEntry)){
          junctionService(junctionEntry)=line.getString("source")
        }
        val p=new Person(line.getString("id"),line.getString("transport"), 
                              line.getInt("speed"),line.getString("source"),
                               line.getString("destination"), line.getBoolean("public_transport"))
        inhabitants(junctionEntry)+=p  
    }
        
    for((junction,inhabitansList) <- inhabitants){
      junctionRegion ! Junction.WrapperMessage(junction,Junction.InitMsgService(junctionService(junction),inhabitansList,20))
    }
      
    
  }
  
  /** Send the public transport line tables (from JSON fils) to the City Junctions.
   */
  def sendPtTables(){
    log.info("sending public transport tables")
    val servicesLines:Map[String,ListBuffer[String]]=Map[String,ListBuffer[String]]()
    val linesStops:Map[String,ListBuffer[String]]=Map[String,ListBuffer[String]]()
    
    
    val url="src/main/resources/ptLines.json"
      
    val lines = scala.io.Source.fromFile(url, "utf-8").getLines.mkString  
      
    val fileLines:JSONArray=new JSONArray(lines)
      
      for(i <- 0 to fileLines.length()-1){
        val line:JSONObject = fileLines.getJSONObject(i)
        val listStops:ListBuffer[String]= ListBuffer[String]()
        val arrJson:JSONArray= line.getJSONArray("listStops");
        for(j <- 0 to arrJson.length()-1){
          listStops+=arrJson.getString(j);
        }
        val id:String=line.getString("id")
        val atTheEnd:String=line.getString("atTheEnd")
        ptLines(id)=(listStops,atTheEnd)
        linesStops(id)=listStops
      }
    
      for(s <- services){
        servicesLines(s)=ListBuffer[String]()
      }
      
      for(ls<-linesStops){
        for(st <- ls._2){
          val stop=stops(st)
          val reachableServices:ListBuffer[String]=stop._2
          for(rs<-reachableServices){
            if(!(servicesLines(rs) contains ls._1)){
              servicesLines(rs)+=ls._1
            }
          }
        }
      }
    
      
      for(j<- junctions){
        junctionRegion ! Junction.WrapperMessage(j,Junction.AddServicesTables(linesStops,servicesLines)) 
        waitingAcks(j)+=1
      }
      

    
     
      
  }
  
  
  def receive = {
    
    case UnreachableMember(member) =>
      log.info("UnreachableMember")
      context.watch(junctionRegion)
      junctionRegion ! ShardRegion.GracefulShutdown
      
    /*Message received from the cluster to indicate a node is UP*/
    case MemberUp(member) =>
      if(!(clusterNodes contains member)){
        clusterNodes+=member
        log.info("Added member")
      }
      if(member.hasRole("central_event_handler")){
           centralEventHandler=context.actorSelection(RootActorPath(member.address) / "user" / CentralEventHandler.name)
      }
      if(clusterNodes.length ==2){
        self ! InitMsg()
      }
      
    case InitMsg() =>
      
      log.info("waiting cluster setup...")
      Thread.sleep(10000)
      log.info("start initialization")

      val actorJunctionProperties = Props[Junction]
          
      
      val alreadyCreatedStops:ListBuffer[String]=ListBuffer[String]()
      val alreadyCreatedServices:ListBuffer[String]=ListBuffer[String]()
      
      /*Build and distribute the city*/
      
      //read and parse the json file containing the city configuration
      val url1="src/main/resources/stops.json"
      
      val lines1 = scala.io.Source.fromFile(url1, "utf-8").getLines.mkString  
      
      val fileStops:JSONArray=new JSONArray(lines1)
      
      //for each link in the array
      for(i <- 0 to fileStops.length()-1){
        val stop:JSONObject = fileStops.getJSONObject(i)
        val id:String= stop.getString("id")
        val stopType:String= stop.getString("type")
        val ptServices:ListBuffer[String]= ListBuffer[String]()
        val arrJson:JSONArray= stop.getJSONArray("ptServices");
        for(j <- 0 to arrJson.length()-1){
          ptServices+=arrJson.getString(j);
        }
        stops(id)=(stopType,ptServices)
        
      }
      
      //read and parse the json file containing the city configuration
      val url="src/main/resources/links.json"
      
      val lines = scala.io.Source.fromFile(url, "utf-8").getLines.mkString  
      
      val ways:JSONArray=new JSONArray(lines)
      
      //for each link in the array
      for(i <- 0 to ways.length()-1){
        //extract the json link object
        val way:JSONObject = ways.getJSONObject(i)
        //take the id of the link 
        //eg. N1_J1-N2_S1 indicates a link from N1_J1 to N2_S1
        val id:String= way.getString("id")
        val source_dest:Array[String]=id.split("-")
        //from the id extract the source of the link
        val source=source_dest(0)
        val destination=source_dest(1)
        
        
        val source_neigh_name:Array[String]=source.split("_")
        val source_neighborhood=source_neigh_name(0)
        val source_name=source_neigh_name(1)
        
        val dest_neigh_name:Array[String]=destination.split("_")
        val dest_neighborhood=dest_neigh_name(0)
        val dest_name=dest_neigh_name(1)
        
        
        if(!(neighborhoods contains source_neighborhood)){
          neighborhoods+=source_neighborhood
        }
        if(!(neighborhoods contains dest_neighborhood)){
          neighborhoods+=dest_neighborhood
        }
        
        
        val sourceType=source_name.charAt(0) match {
          case 'J' => "junction"
          case 'S' => "service"
          case 'H' => "service"
          case 'W' => "service"
          case 'B' => "stop"
          case 'T' => "stop"
        }

        
        val destinationType=dest_name.charAt(0) match {
          case 'J' => "junction"
          case 'S' => "service"
          case 'H' => "service"
          case 'W' => "service"
          case 'B' => "stop"
          case 'T' => "stop"
        }
        
        
        
        val linkWays:Map[String,Tuple2[Vector2,Vector2]]=Map[String,Tuple2[Vector2,Vector2]]()      
        
        
        for(wayType <- Utils.transportWaysType){
          if(!way.isNull(wayType)){
            val wayMap=way.getJSONObject(wayType)
            val x1:Int=wayMap.getInt("x1")
            val y1:Int=wayMap.getInt("y1")
            val x2:Int=wayMap.getInt("x2")
            val y2:Int=wayMap.getInt("y2")
            linkWays(wayType)=new Tuple2(new Vector2(x1,y1), new Vector2(x2,y2))
          }
        }
        
              
        sourceType match {
          case "junction" =>
                if(!(junctions contains source)){
                  junctions+=source
                }
          case "service" =>
                if(!(services contains source)){
                  services+=source
                  junctions+=source
                }
        }
        
        
        
        destinationType match {
          case "junction" =>
                if(!(junctions contains destination)){
                  junctions+=destination
                }
          case "service" =>
                if(!(services contains destination)){
                  services+=destination
                  junctions+=destination
                }
        }
        

        
        val sourceLinkWays:Map[String,Vector2]=Map[String,Vector2]()
        val destLinkWays:Map[String,Vector2]=Map[String,Vector2]()
        
        for(wayType <- Utils.transportWaysType){
          if(linkWays.contains(wayType)){
            sourceLinkWays(wayType)=linkWays(wayType)._1
            destLinkWays(wayType)=linkWays(wayType)._2
          }
          else{
            sourceLinkWays(wayType)=null
            destLinkWays(wayType)=null
          }
        }

        links+=id
        
        
        var toAdd=1
        junctionRegion ! Junction.WrapperMessage(source,Junction.AddLinkOUT(destination, sourceLinkWays))
        if((stops contains source) && (!(alreadyCreatedStops contains source))){
          junctionRegion ! Junction.WrapperMessage(source,Junction.AddStop(stops(source)._1,stops(source)._2))
          toAdd=2
          alreadyCreatedStops+=source
        }
        if((services contains source) && (!(alreadyCreatedServices contains source))){
          junctionRegion ! Junction.WrapperMessage(source,Junction.AddService())
          toAdd=2
          alreadyCreatedServices+=source
        }
        if(waitingAcks contains source){
              waitingAcks(source)+=toAdd
        }
        else{
          waitingAcks(source)=toAdd
        }
        
        
        
        junctionRegion ! Junction.WrapperMessage(destination,Junction.AddLinkIN(source, destLinkWays, linkWays))
        if(waitingAcks contains destination){
                  waitingAcks(destination)+=1
        }
        else{
                  waitingAcks(destination)=1
        }
  
     
      }
      
      /*End Build and distribute the city*/
      
      
      /*Build routing tables*/
      
      val allNodes:ListBuffer[String]=ListBuffer[String]()
      val allEdges:ListBuffer[String]=ListBuffer[String]()
      
      val wayNodes:Map[String,ListBuffer[String]]=
      Map("paviment" -> ListBuffer[String](),
          "bikelane" -> ListBuffer[String](),
          "street" -> ListBuffer[String](),
          "tramlane" -> ListBuffer[String]())
      
      val wayEdges:Map[String,ListBuffer[String]]=
      Map("paviment" -> ListBuffer[String](),
          "bikelane" -> ListBuffer[String](),
          "street" -> ListBuffer[String](),
          "tramlane" -> ListBuffer[String]())

      val wayGraphs:Map[String,Graph]=
      Map("paviment" -> new SingleGraph("paviment"),
          "bikelane" -> new SingleGraph("bikelane"),
          "street" -> new SingleGraph("street"),
          "tramlane" -> new SingleGraph("tramlane"))


      for(i <- 0 to ways.length()-1){
        val way:JSONObject = ways.getJSONObject(i)
        //take the id of the link 
        //eg. N1_J1-N2_S1 indicates a link from N1_J1 to N2_S1
        val id:String= way.getString("id")
        val source_dest:Array[String]=id.split("-")
        //from the id extract the source of the link
        val source:String=source_dest(0)
        val destination:String=source_dest(1)
          
        for(wayType <- Utils.transportWaysType){
          if(!way.isNull(wayType)){
              val wayMap=way.getJSONObject(wayType)
              val x1:Int=wayMap.getInt("x1")
              val y1:Int=wayMap.getInt("y1")
              val x2:Int=wayMap.getInt("x2")
              val y2:Int=wayMap.getInt("y2")
              val direction:String=wayMap.getString("direction")
              if(!(wayNodes(wayType) contains source)){
                wayGraphs(wayType).addNode(source)
                wayNodes(wayType)+=source
              }
              if(!(wayNodes(wayType) contains destination)){
                wayGraphs(wayType).addNode(destination)
                wayNodes(wayType)+=destination
              }
              if(!(wayEdges(wayType) contains (source+"-"+destination))){
                var length:Int=0
                if(direction=="up" || direction=="down"){
                  length=Math.abs(y1-y2)
                }
                if(direction=="right" || direction=="left"){
                  length=Math.abs(x1-x2)
                }
                if((stops contains source) || (stops contains destination)){
                  length+=(2*16)
                }
                val e:Edge=(wayGraphs(wayType)).addEdge(source+"-"+destination,source,destination)
                wayEdges(wayType)+=(source+"-"+destination)
                wayEdges(wayType)+=(destination+"-"+source)   
                e.addAttribute("length", Int.box(length))
              }
          }
        }
      }
      

      for(wayType<-Utils.transportWaysType){
        val g:Graph=wayGraphs(wayType)
        val it: Iterable[Node] = g.getEachNode.asScala
		    val it2: Iterable[Edge] = g.getEachEdge.asScala
        
      }
  
      for(wayType<-Utils.transportWaysType){
        val g:Graph=wayGraphs(wayType)
        val dijkstra:Dijkstra = new Dijkstra(Dijkstra.Element.EDGE, null, "length");
	      dijkstra.init(g);
	      for(n <- wayNodes(wayType)){
	        dijkstra.setSource(g.getNode(n));
		      dijkstra.compute();
		      for(n2<-wayNodes(wayType)){
		        if(n2!=n){
		          val myPath:Path=dijkstra.getPath(g.getNode(n2))
		          val pathLength=dijkstra.getPathLength(g.getNode(n2))
		          val it: Iterable[Edge] = myPath.getEachEdge.asScala
		          val it2: Iterable[Node] = myPath.getEachNode.asScala
		          var i=0
		          for(el <- it2){
		            if(i==1){
		              if(!(routingTablesWayType(wayType) contains n)){
		                routingTablesWayType(wayType)(n)=Map[String,Tuple2[String,Double]]()
		              }
		              routingTablesWayType(wayType)(n)(n2)=(el.toString(),pathLength)
		            }
		            i+=1
		          }
		          
		        }
		        
		      }
	      }
	    }
      
      /*End Build routing tables*/
      
      sendPtTables()
      
            

     /*Message received from a Junction to indicate it (and its Children) changed behavior*/
     case StopAckMsg() =>
        waitingStopAcks-=1
        if(waitingStopAcks==0){
          centralEventHandler ! CentralEventHandler.StopMsg()
          //Thread.sleep(5000)
          log.info("FRONTEND SENT STOP TO CENTRAL EVENT HANDLER")
          
        }
      
     /*Message received from an entity to indicate it received the initial configuration*/
     case AckMsg(source) =>
       waitingAcks(source)-=1
       if(waitingAcks(source)==0){
       }
       ready=true
       breakable{
         for(w <- waitingAcks){
           if(w._2 != 0){
             ready=false
             break
           }
         }
       }
       if(ready && routing){        
        var counterBus=0
        var counterTram=0
                 
        for(l<-ptLines){
          val lineStops:ListBuffer[String]=(l._2)._1
          val firstStop:String=lineStops(0)
          val stopType=stops(firstStop)._1
          val atTheEnd:String=(l._2)._2
          var ptVehicle:PublicTransportVehicle=null
          if(stopType=="bus_stop"){
            ptVehicle=new Bus("B"+counterBus,15,lineStops,l._1,atTheEnd)
            counterBus+=1
          }
          if(stopType=="tram_stop"){
            ptVehicle=new Tram("T"+counterTram,20,lineStops,l._1,atTheEnd)
            counterTram+=1
          }
          
          junctionRegion ! Junction.WrapperMessage(firstStop,Junction.InitMsgStop(stopType,ListBuffer(ptVehicle)))
        } 
         
        sendInhabitants()

        
        Thread.sleep(10000)
        log.info("SYSTEM START")
        for(j<- junctions){
          junctionRegion ! Junction.WrapperMessage(j,Junction.StartMsg())
        }   
      
                
        
        
       }
       else{
         if(ready){
           log.info("SEND ROUTING TABLES")
           sendRoutingTables()
         }
       }       
     
     /*Message received from the CentralEventHandler to indicate the last frame was received*/
     case StopMsg() =>
        log.info("STOP SYSTEM REQUEST")
        for(j<- junctions){
          junctionRegion ! Junction.WrapperMessage(j,Junction.StopMsg())
          waitingStopAcks+=1
        }
       
     
     case default =>
       log.info(s"Received an Unhandled message from ${sender} -> ${default}")
       
    }
  

}

object Frontend {  
  case class InitMsg()
  case class AckMsg(source:String)
  case class StartMsg()
  case class StopAckMsg()
  case class StopMsg()
  val name:String="frontend"
}