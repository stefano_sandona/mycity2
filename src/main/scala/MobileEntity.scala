import Utils._
import java.util.Random
import org.json._

/** A Mobile Entity.
 * 
 *  @constructor create a new mobile entity with an id, transportType, speed and destination
 *  @param id the unique ID of the mobile entity
 *  @param transportType the type of the transport by which the entity moves
 *  @param speed the standard speed of the mobile entity
 *  @param dest the actual destination of the mobile entity
 */

abstract class MobileEntity(val id: String, val transportType: String, val speed:Int, var dest: String) extends Serializable {
    
  
    def toJson():JSONObject

    var actualPositionVect:Vector2=null    //coordinates of the entity
    
    //actualEntryVect,actualExitVect to calculate the progress vector
    var actualExitVect:Vector2=null        //coordinates of the entity's exit point (on the link / junction)
    var actualEntryVect:Vector2=null       //coordinates of the entity's entry point (on the link / junction)
    
    var distanceToEnd:BigDecimal=0        //distance to the end of the trajectory inside the junction/link (to order the entities on the queue)
    var way:String=null                   //way type = tramlane, street, bikelane, paviment
    var status=statusType("sleeping")     //status of the entity (sleeping, waiting, traveling, ready, serving)
    
    if(transportType=="feet"){
      way="paviment"
    }
    else{
      if(transportType=="car" || transportType=="bus"){
        way="street"
      }
      else{
        if(transportType=="tram"){
          way="tramlane"
        }
        else{
          if(transportType=="bike"){
            way="bikelane"
          }
          else{
            throw new Error("Unknown transport type!")
          }
          
        }
      }
    }
    
    
    val length=transport_size.get(transportType).get  //total occupation of the entity (length/2 front and length/2 bottom)
  }