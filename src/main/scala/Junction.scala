import akka.actor.{Props, Actor, ActorSystem, ActorRef,ActorSelection,ActorLogging};
import scala.concurrent.duration._
import akka.cluster.sharding.ShardRegion
import akka.persistence.PersistentActor
import akka.persistence.Recovery
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map;
import akka.cluster.sharding.ClusterSharding
import scala.math._;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import org.json._


/** A Junction.
 * 
 *  @constructor create a new Junction
 */

class Junction extends PersistentActor with ActorLogging with Utils.ReaperWatched{
  import Junction._
    
  //Junction ID
  val ID=(self.path.parent.name)+'_'+(self.path.name)
  
  //Neighborhood ID
  val actualNeighborhood=ID.split("_")(0)
  
  //ID for the persistence
  override def persistenceId: String = self.path.parent.name + "-" + self.path.name
  
  //Linked Junctions
  var neighboors:ListBuffer[String]=ListBuffer[String]()
  
  //Shard Region Actor
  val junctionRegion: ActorRef = ClusterSharding(context.system).shardRegion(Junction.shardName)
  
  //Local Event Handler
  var eventHandler:ActorSelection=context.actorSelection("/user/eventHandler");

  //Frontend actor
  var frontend:ActorRef=null
  
  //entity to ack
  var toAckMobileEntity:ListBuffer[MobileEntity]=ListBuffer[MobileEntity]()
  var toAckSender:ListBuffer[ActorRef]=ListBuffer[ActorRef]()
  
  //Number of waiting acks to stop
  var waitingAcks:Int=0
  
  /*Distributed frame correlation algorithm variables*/
  
  //actual frameID
  var frame:Long=0
  
  //Number of waiting acks to proceed the computation from the linked Junctions
  var frameWaitingNeighboors:Map[Long,Int]=Map[Long,Int]()
  
  //Number of waiting acks to proceed the computation from the Children
  var frameWaitingChildren:Map[Long,Int]=Map[Long,Int]()
  
  //indicate if the computation can proceed or not
  var waitingMode:Boolean=false
  
  //indicate if an Ack was already sent to the Children
  var alreadySentChildren:Boolean=false
  
  //indicate if an Ack was already sent to the linked Junctions
  var alreadySentNeighboors:Boolean=false
  
  //indicate if the actual frame situation was already sent to the EventHandler
  var alreadySentEvent:Boolean=false
  
  /*End Distributed frame correlation algorithm variables*/
  
  //Prevent the recovery from the journal
  override def recovery = Recovery.none
  
  /*Children*/

  var links:Map[String,ActorRef]=Map[String,ActorRef]()
  var stop:ActorRef=null
  var service:ActorRef=null

  /*End Children*/

  /*Routing*/
  
  //Map[wayType, Map[Destination, (nextJunction,distance)]]
  var routingTablesWayType:Map[String,Map[String,Tuple2[String,Double]]]=Map[String,Map[String,Tuple2[String,Double]]]()
  
  /*End Routing*/

  /*Public trasports data*/

  //Map[lineID, List(stops)]]
  var linesStops:Map[String, ListBuffer[String]]=null

  //Map[serviceID, List(lineIDs)]]
  var servicesLines:Map[String,ListBuffer[String]]=null

  /*End Public trasports data*/
  
  
  /*IN and OUT links*/
  
  //Map[nearJunction, Map[wayType, (linkActor,EntryPoint])]
  var inLinks:Map[String, Map[String,Tuple2[ActorRef,Vector2]]]=Map[String, Map[String,Tuple2[ActorRef,Vector2]]]()
  
  //Map[nearJunction, Map[wayType, ExitPoint]]
  var outLinks:Map[String, Map[String,Vector2]]=Map[String, Map[String,Vector2]]()

  /*End IN and OUT links*/

  
  context.setReceiveTimeout(120.seconds)

  
  
  /*Mobile entities*/
  
  //entities that are waiting to cross the junction
  //Map[wayType, List(MobileEntity, SenderActor, NextJuncion)]
  val waitingEntities:Map[String,ListBuffer[Tuple3[MobileEntity,ActorRef,String]]]=
                                                Map("tramlane" -> ListBuffer[Tuple3[MobileEntity,ActorRef,String]](),
                                                    "paviment" -> ListBuffer[Tuple3[MobileEntity,ActorRef,String]](),
                                                    "bikelane" -> ListBuffer[Tuple3[MobileEntity,ActorRef,String]](),
                                                     "street" -> ListBuffer[Tuple3[MobileEntity,ActorRef,String]]())
    
        
  val readyEntities=Map[String,MobileEntity]()	//entities that reached the end of their path inside the junction
  val movingEntities=ListBuffer[MobileEntity]()	//entities that are moving inside the junction
  
  val servingEntities:Map[String,Tuple3[MobileEntity,ActorRef,String]]=
                                                 Map("tramlane" -> null,
                                                    "paviment" -> null,
                                                    "bikelane" -> null,
                                                     "street" -> null)

  /*End Mobile entities*/
                                                 
  //trajectories of the crossing mobile entities (to check if another entity can cross)
  //Map[mobileEntityID, trajectory]
  val trajectories= Map[String,Line2D.Double]()
    
  
  
   
  /** Retrieve the next JunctionID from the routing tables.
   * 
   *  @param mobile entity
   *  @return a tuple with the next JunctionID and the local Exit Point 
   */  
  def getNextHop(mobileEntity:MobileEntity):Tuple2[String,Vector2]={
      var dest=mobileEntity.dest
      
      var routingTable:Map[String,Tuple2[String,Double]]=null
      mobileEntity.way match{
        case "tramlane" => routingTable= routingTablesWayType("tramlane")
        case "street" => routingTable= routingTablesWayType("street")
        case "bikelane" => routingTable= routingTablesWayType("bikelane")
        case "paviment" => routingTable= routingTablesWayType("paviment")
      }
      if (routingTable==None){
          throw new Exception("RoutingTable ["+mobileEntity.way+"] not present")
      }
      
      if (mobileEntity.isInstanceOf[Person]){
        //if the person wants to use public transports and has not already took a public transport vehicle
        if(mobileEntity.asInstanceOf[Person].publicTransport && !mobileEntity.asInstanceOf[Person].alreadyTook){
          //get the public transport route number to reach the destination
          val routes:ListBuffer[String]=servicesLines(mobileEntity.dest)
          val allStops:ListBuffer[String]=ListBuffer[String]()
          val allStopsDistance:ListBuffer[Double]=ListBuffer[Double]()
          //get the Public Transport Stops that the route cross
          for(r <- routes){
            allStops++=linesStops(r)
          }
          //get the Public Transport Stops distance
          for(s<-allStops){
            allStopsDistance+=routingTable(s)._2
          }
          //find the closest PublicTransportStop
          val indexMin:Int=allStopsDistance.zipWithIndex.minBy(_._1)._2
          
          //change the actual destination
          dest=allStops(indexMin)
         
          //set the next public transport line to reach
          for(r <- routes){
            if(linesStops(r) contains dest){
              mobileEntity.asInstanceOf[Person].lineID=r
            }
          }
        }
      }
      
      
      if(routingTable contains dest){
        var nextHop:String=routingTable(dest)._1
        val exitPoint:Vector2=(outLinks(nextHop))(mobileEntity.way)
        return (nextHop, exitPoint)
      }
      else{
         val nextHop=mobileEntity.dest
         val exitPoint:Vector2=(outLinks(nextHop))(mobileEntity.way)
         return (nextHop, exitPoint)
      }
    }
    
    
   /** Check if a trajectory collide with ones of the crossing mobile entities
    * 
    *  @param mobile entity trajectory
    *  @return true if the trajectory collide, false otherwise
    */ 
    def checkCollision(line: Line2D):Boolean={
      for(elem <- trajectories){ 
        if(line.intersectsLine(elem._2)){
          return true
        }
      }
      return false
    }
    
   /** Check each waiting entity of a particular waytype if it can start to cross the junction
    * 
    *  @param waytype
    */ 
    def checkWayWaitingEntity(way:String){
      val wayWaitingEntities=waitingEntities(way)
      for(elem <- wayWaitingEntities){
        val mobileEntity:MobileEntity = elem._1
        val sender:ActorRef = elem._2
        val exitActorID:String = elem._3
        val trajectory = new Line2D.Double(mobileEntity.actualPositionVect.x.toDouble, 
                                          mobileEntity.actualPositionVect.y.toDouble,
                                          mobileEntity.actualExitVect.x.toDouble,
                                          mobileEntity.actualExitVect.y.toDouble)
      
        /*if the trajectory does not intersect another one, the entity starts to move*/
        if(!checkCollision(trajectory)){
          trajectories(mobileEntity.id) = trajectory
          
          toAckMobileEntity+=mobileEntity
          toAckSender+=sender


          self ! TravelMsgJ(mobileEntity,exitActorID)
          movingEntities+=mobileEntity
          waitingEntities(way)-=elem
        }
      }
    }
    
    
   /** Check each waiting entity if it can start to cross the junction.
    */ 
    def checkWaitingEntities(){
      val keys=waitingEntities.keys
      for(key <- keys){
        checkWayWaitingEntity(key)
      }
    }
    
        
    
    

   /** Check if an entity reached the end of its local crossing path with the given movement
    * 
    *  @param start Point of the movement
    *  @param end point of the movement
    *  @param end point of the path
    *  @return true if the entity reached the end of the crossing path, false otherwise
    */ 
    def checkReachedPoint(start:Vector2, end:Vector2, exit:Vector2):Boolean={
      val d1=start.getDistance(exit)
      val d2=start.getDistance(end)
        if(d1<=d2){
          return true
        }
        else{
          return false
        }
    }

  /** Creates a JSON object that contains a summary of the owned mobile entities.
    * 
    *  @return the summary JSON object
    */ 
    def summary():JSONObject={
      val json:JSONObject=new JSONObject()
      var entities:JSONArray=new JSONArray()
      for(el <- readyEntities){
          val e:JSONObject=(el._2).toJson()
          if(el==servingEntities((el._2).way)){
            e.put("serving",true)
          }
          else{
            e.put("serving",false)
          }
          entities.put(e)
      }
      for(el <- movingEntities){
          val e:JSONObject=el.toJson()
          e.put("serving",false)
          entities.put(e)
      }
      json.put("id",ID)
      if(stop == null && service==null){
        json.put("type","junction");
      }
      else{
      	if(stop != null){
        	json.put("type","stop");
      	}
      	else{
      		json.put("type","service");
      	}
      }
      json.put("entities",entities)
      return json
    }
    
    
    
    
    
   /** Move a mobile entity.
    * 
    *  @param mobile entity
    *  @return true if the entity reached the end of the crossing path, false otherwise
    */ 
    def moveEntity(mobileEntity: MobileEntity):Boolean={  
      var readyToBeServed=false

      val linkVector=mobileEntity.actualEntryVect.getDistanceVector(mobileEntity.actualExitVect)
    
      val linkLength =linkVector.getNorm()	//length of the link
      val linkRealLength =linkLength*Utils.unit //length of the link in meters
        
      val numberOfTimeUnits=linkRealLength/mobileEntity.speed	//number of time units to complete
      
      val actualDirection=mobileEntity.actualPositionVect.getDistanceVector(mobileEntity.actualExitVect)
        
      val progr:Vector2=actualDirection.getVectOfLenght(linkLength/numberOfTimeUnits) //progress vector per time unit
      
      
      var possibleNextPoint = mobileEntity.actualPositionVect.add(progr)
      
      if(checkReachedPoint(mobileEntity.actualPositionVect,
                            possibleNextPoint, 
                            mobileEntity.actualExitVect)){
        possibleNextPoint=mobileEntity.actualExitVect
        readyToBeServed=true
      }
      
      mobileEntity.actualPositionVect=possibleNextPoint
      mobileEntity.distanceToEnd = (mobileEntity.actualExitVect).getDistance(mobileEntity.actualPositionVect)
      
      val trajectory = new Line2D.Double(mobileEntity.actualPositionVect.x.toDouble, 
                                          mobileEntity.actualPositionVect.y.toDouble,
                                          mobileEntity.actualExitVect.x.toDouble,
                                          mobileEntity.actualExitVect.y.toDouble)
      
      trajectories-=(mobileEntity.id)
      trajectories(mobileEntity.id) = trajectory    
      
      return readyToBeServed
             
    }
  


   /** Add the given links as input links
    * 
    *  @param event 
    */ 
  def updateState(event: AddLinkIN_Evt): Unit ={
    for(linkway <- event.linkways){
      val linkType=linkway._1
      val startVect=linkway._2._1
      val endVect=linkway._2._2
      val link:ActorRef=(context.system).actorOf(Props(new Link(startVect, endVect,self)), name = event.actorFromID+'-'+ID+'_'+linkType)
      links(event.actorFromID+'-'+ID+'_'+linkType)=link
      if(inLinks contains event.actorFromID){
        inLinks(event.actorFromID)(linkType)=(link,endVect)
      }
      else{
        inLinks(event.actorFromID)=Map[String, (ActorRef,Vector2)]()
        inLinks(event.actorFromID)(linkType)=(link,endVect)
        
        val source_name_arr:Array[String]=event.actorFromID.split("_")
        val source_neighborhood=source_name_arr(0)
        val source_name=source_name_arr(1)
        neighboors+=event.actorFromID
      }
      
    }
    sender() ! Frontend.AckMsg(ID)
  }
  
  
   /** Add the given links as output links
    * 
    *  @param event 
    */ 
  def updateState(event: AddLinkOUT_Evt): Unit ={
    outLinks(event.actorToID)=event.linkOUTWays
    sender() ! Frontend.AckMsg(ID)
  }

  
   /** Add a stop
    * 
    *  @param event 
    */ 
  def updateState(event: AddStop_Evt): Unit ={
    val stopActor:ActorRef=(context.system).actorOf(Props(new PublicTransportStop(self,event.stopType, event.ptServices)), name = ID+"-"+event.stopType)
    stop=stopActor
    sender() ! Frontend.AckMsg(ID)
  }
  
   /** Add a service
    * 
    *  @param event 
    */ 
  def updateState(event: AddService_Evt): Unit ={
    val serviceActor:ActorRef=(context.system).actorOf(Props(new Service(self)), name = ID+"-service")
    service=serviceActor
    sender() ! Frontend.AckMsg(ID)
  }
  
  /** Add the routing table
    * 
    *  @param event 
    */ 
  def updateState(event: AddRoutingTable_Evt): Unit ={
    routingTablesWayType(event.way)=event.routingTable
    sender() ! Frontend.AckMsg(ID)
  }
  
  /** Add the public transport tables
    * 
    *  @param event 
    */ 
  def updateState(event: AddServicesTables_Evt): Unit ={
    linesStops=event.linesStops
    servicesLines=event.servicesLines
    
    sender() ! Frontend.AckMsg(ID)
  }

  override def receiveRecover: Receive = {
    case evt: AddLinkIN_Evt ⇒ updateState(evt)
    case evt: AddLinkOUT_Evt ⇒ updateState(evt)
    case evt: AddStop_Evt ⇒ updateState(evt)
    case evt: AddService_Evt ⇒ updateState(evt)
    case evt: AddRoutingTable_Evt ⇒ updateState(evt)
    case evt: AddServicesTables_Evt ⇒ updateState(evt)
  }

  override def receiveCommand=start
  
  def end: Receive = {       
    case _  =>
      //log.info(s"throw away message from ${sender}")
  }

  def start: Receive = {       
    
    /*Message that indicates the end of the previous time frame*/
    case Utils.FrameMsg(id:Long) =>
      if(!(alreadySentEvent)){
        eventHandler ! Utils.EventMsg(frame,summary())
        alreadySentEvent=true
      }
      
      if(!(frameWaitingNeighboors contains frame)){
        frameWaitingNeighboors(frame)=neighboors.length
      }
      
      if(!(frameWaitingChildren contains frame)){
        frameWaitingChildren(frame)=links.size
        if(service!=null){
          frameWaitingChildren(frame)+=1
        }
        if(stop!=null){
          frameWaitingChildren(frame)+=1
        }
      }
      
      if(!(frameWaitingChildren(frame)==0) || !(frameWaitingNeighboors(frame)==0) || !alreadySentChildren || !alreadySentNeighboors || !alreadySentEvent){
        waitingMode=true
      }
      
      if(frameWaitingChildren(frame)==0 && !alreadySentNeighboors){
        for(n<-neighboors){
          junctionRegion ! WrapperMessage(n, Utils.FrameSentMsg(frame))
        }
        alreadySentNeighboors=true
      }
      
      if(frameWaitingNeighboors(frame)==0 && frameWaitingChildren(frame)==0 && !alreadySentChildren){
        if(service!=null){
          service ! Utils.FrameSentMsg(frame)
        }
        if(stop!=null){
          stop ! Utils.FrameSentMsg(frame)
        }
        for(l<-inLinks){
          for(w<-l._2){
            (w._2)._1 ! Utils.FrameSentMsg(frame)
          }
        }
        alreadySentChildren=true
      }
      
      if((frameWaitingChildren(frame)==0) && (frameWaitingNeighboors(frame)==0) && alreadySentChildren && alreadySentNeighboors && alreadySentEvent){
        frameWaitingChildren-=frame
        frameWaitingNeighboors-=frame
        waitingMode=false
        frame+=1
        if(!(frameWaitingNeighboors contains frame)){
          frameWaitingNeighboors(frame)=neighboors.length
        }
        if(!(frameWaitingChildren contains frame)){
          frameWaitingChildren(frame)=links.size
          
          if(service != null){
            frameWaitingChildren(frame)+=1
          }
          
          if(stop != null){
            frameWaitingChildren(frame)+=1
          }
          
        }
        alreadySentChildren=false
        alreadySentNeighboors=false
        alreadySentEvent=false
      }
      self ! Utils.FrameMsg(frame)
      

      /*Messages received from a linked junction that indicates that it and its children have sent
       * their status to the Event Handler*/
      case Utils.FrameSentMsg(id:Long) =>
        if(!(frameWaitingNeighboors contains id)){
          frameWaitingNeighboors(id)=neighboors.length-1
        }
        else{
          frameWaitingNeighboors(id)-=1
        }
        
      /*Message received from a Children that indicates that it has sent its status to the Event Handler*/
      case Utils.ChildFrameSentMsg(id:Long) =>
        if(!(frameWaitingChildren contains id)){
          frameWaitingChildren(id)=(links.size)-1
          
          if(service!=null){
            frameWaitingChildren(id)+=1
          }
          if(stop!=null){
            frameWaitingChildren(id)+=1
          }
        }
        else{
          frameWaitingChildren(id)-=1
        }
        
      /*Message received from the frontend to start the Junction and its Children*/
      case StartMsg() =>
        log.info(s"StartMsg JUNCTION")
         if(service!=null){
          service ! Service.StartMsg()
        }
        if(stop!=null){
          stop ! PublicTransportStop.StartMsg()
        }
        for(l<-inLinks){
          for(w<-l._2){
            (w._2)._1 ! Link.StartMsg()
          }
        }
        self ! Utils.FrameMsg(frame)
      
      
    /*Initial Setting messages*/
        
    case AddLinkIN(actorFromID,linkINways,linkways) =>
      persist(AddLinkIN_Evt(actorFromID,linkINways,linkways))(updateState)
    
    case AddLinkOUT(actorToID,linkOUTways) =>
      persist(AddLinkOUT_Evt(actorToID,linkOUTways))(updateState)   
      
    case InitMsgService(destID,customers, permanenceTime) =>
      service ! Service.InitMsg(customers,permanenceTime)
      
    case InitMsgStop(stopType,ptVehicles) =>
      stop ! PublicTransportStop.InitMsg(ptVehicles)
    
    case AddStop(stopType:String, ptServices:ListBuffer[String]) =>
      persist(AddStop_Evt(stopType,ptServices))(updateState)
      
    case AddService() =>
      persist(AddService_Evt())(updateState)
    
    case AddRoutingTable(way:String,routingTable:Map[String,Tuple2[String,Double]]) =>
      persist(AddRoutingTable_Evt(way,routingTable))(updateState)
    
    case AddServicesTables(linesStops:Map[String, ListBuffer[String]],servicesLines:Map[String,ListBuffer[String]]) =>
      persist(AddServicesTables_Evt(linesStops,servicesLines))(updateState)
      
   /*End Initial Setting messages*/
      
    
    /*Ack Message received from a child to indicate it changed behavior */
    case Utils.StopAckMsg()=>
        waitingAcks-=1
        if(waitingAcks==0){
          frontend ! Frontend.StopAckMsg()
          log.info("StopMsg JUNCTION")
          context.become(end)
        }
        
    /*Message received from the frontend to stop the Junction and its children */
    case StopMsg()=>
        frontend=sender()
        if(service!=null){
          service ! Service.StopMsg()
          waitingAcks+=1    //PROBLEM
        }
        
        if(stop!=null){
           stop ! PublicTransportStop.StopMsg()
           waitingAcks+=1    //PROBLEM
        }
        
        for(link <- inLinks){
          for(way<-link._2){
            (way._2)._1 ! Link.StopMsg()
            waitingAcks+=1
          }
        }
    
    /*Each Message receivedduring waiting mode is requeued */   
    case msg:Any if waitingMode =>
      self forward msg 
      
    /*Message received from an INPUT link to accept a mobile entity */
    case Utils.EnterMsg(senderID,mobileEntity) if ((stop == null)&&(service == null)) =>
        val senderID=sender.path.name
        //retrieve next hop and calculate the trajectory
        val (exitActor, exitVect)=getNextHop(mobileEntity)
        
        mobileEntity.actualEntryVect=mobileEntity.actualPositionVect
        mobileEntity.actualExitVect=exitVect
        mobileEntity.distanceToEnd = (mobileEntity.actualExitVect).getDistance(mobileEntity.actualPositionVect)
        
        
        mobileEntity.status=Utils.statusType("waiting")
        
        val tup:Tuple3[MobileEntity,ActorRef,String]=(mobileEntity, sender, exitActor)
        waitingEntities(mobileEntity.way)+=tup
                
        //check if leave the entity in the waiting entities or accept it
        checkWaitingEntities()
    
    /*Message received from an INPUT link to accept a mobile entity */
    case Utils.EnterMsg(senderID,mobileEntity) if (stop != null) =>
        stop forward Utils.EnterMsg(senderID,mobileEntity)
        
    /*Message received from an INPUT link to accept a mobile entity */
    case Utils.EnterMsg(senderID,mobileEntity) if (service != null) =>
        service forward Utils.EnterMsg(senderID,mobileEntity)
        
    /*Message received from a stop to accept a mobile entity */
    case Utils.EnterFromStopMsg(senderID,mobileEntity) if (stop != null) =>
        val senderID=sender.path.name
        val (exitActor, exitVect)=getNextHop(mobileEntity)
        
        mobileEntity.actualPositionVect=exitVect
        if(servingEntities(mobileEntity.way)!=null){
          log.info("PROBLEM A")
        }
        servingEntities(mobileEntity.way)=(mobileEntity, sender, exitActor)
        mobileEntity.status=Utils.statusType("serving")
        
        junctionRegion ! WrapperMessage(exitActor, Utils.AcceptMsg(ID,mobileEntity))

        
        
    /*Message received from a stop to accept a mobile entity */
    case Utils.EnterFromServiceMsg(senderID,mobileEntity) if (service != null) =>
        val senderID=sender.path.name
        val (exitActor, exitVect)=getNextHop(mobileEntity)
        
        mobileEntity.actualPositionVect=exitVect
        
        if(servingEntities(mobileEntity.way)!=null){
          log.info("PROBLEM B")
        }
        servingEntities(mobileEntity.way)=(mobileEntity, sender, exitActor)
        mobileEntity.status=Utils.statusType("serving")
        
        junctionRegion ! WrapperMessage(exitActor, Utils.AcceptMsg(ID,mobileEntity))
        
     
        
        
        
    /*Message received from a linked Junction to accept a mobile entity */
    case Utils.AcceptMsg(senderID,mobileEntity) =>
      inLinks(senderID)(mobileEntity.way)._1 forward Utils.AcceptMsg(senderID,mobileEntity)
        
    /*Message received from itself to move the mobile entity */
    case TravelMsgJ(mobileEntity, exitActorID) =>
        val readyToBeServed:Boolean=moveEntity(mobileEntity: MobileEntity)

        
        var index=toAckMobileEntity.indexOf(mobileEntity)
        if(index != -1 && !(mobileEntity.actualPositionVect.equals(mobileEntity.actualEntryVect))){
          toAckSender(index) ! Utils.AcceptedMsg(mobileEntity.id, mobileEntity.way)
          toAckMobileEntity.remove(index)
          toAckSender.remove(index)
        }
        index=toAckMobileEntity.indexOf(mobileEntity)
        if(index != -1){
          log.info("PROBLEM C")
        }
        


        /*if the entity is ready to be served it is sent to the next Junction*/
        if(readyToBeServed){
          mobileEntity.status=Utils.statusType("serving")
          readyEntities(mobileEntity.id)=mobileEntity
          movingEntities-= mobileEntity
          junctionRegion ! WrapperMessage(exitActorID, Utils.AcceptMsg(ID,mobileEntity))
     
        }
        else{
          self ! TravelMsgJ(mobileEntity, exitActorID)
        }
        
        //check if we can accept someone
        checkWaitingEntities()
        
        
      /*Ack message to confirm the acceptance of a sent mobile entity */
      case Utils.AcceptedMsg(idx, way) if (stop==null)&&(service==null) =>
          val elem=readyEntities(idx)
          readyEntities-= idx
          trajectories -= idx  
          //check if we can accept someone
          checkWaitingEntities()
          
      case Utils.AcceptedMsg(idx,way) if (stop!=null)||(service!=null)=>
        val (mobileEntity,sender,exitActor)=servingEntities(way)
        sender ! Utils.AcceptedMsg(mobileEntity.id,mobileEntity.way)
        servingEntities(way)=null

    
    case default =>
        throw new Exception("["+ID+"] Received a strange message from "+sender()+" -> "+default)
        
  }
}

object Junction {
  
  trait Command
  case class AddLinkOUT(actorToID:String, linkOUTWays:Map[String,Vector2]) extends Command
  case class AddLinkIN(actorFromID:String, linkINWays:Map[String,Vector2], linkways:Map[String, (Vector2, Vector2)]) extends Command
  case class TravelMsgJ(mobileEntity:MobileEntity, exitaActorID:String)
  case class InitMsgService(destID:String,customers:ListBuffer[Person], permanenceTime:Int) extends Command
  case class InitMsgStop(stopType:String,ptVehicles:ListBuffer[PublicTransportVehicle]) extends Command
  case class AddStop(stopType:String, ptServices:ListBuffer[String]) extends Command
  case class AddService() extends Command
  case class StartMsg() extends Command
  case class StopMsg() extends Command
  case class AddRoutingTable(way:String,routingTable:Map[String,Tuple2[String,Double]]) extends Command
  case class AddServicesTables(linesStops:Map[String, ListBuffer[String]],servicesLines:Map[String,ListBuffer[String]] ) extends Command
  
  trait Event
  case class AddLinkIN_Evt(actorFromID:String, linkINWays:Map[String,Vector2], linkways:Map[String, (Vector2, Vector2)]) extends Event
  case class AddLinkOUT_Evt(actorToID:String, linkOUTWays:Map[String,Vector2]) extends Event
  case class AddStop_Evt(stopType:String, ptServices:ListBuffer[String]) extends Event
  case class AddService_Evt() extends Event
  case class AddRoutingTable_Evt(way:String,routingTable:Map[String,Tuple2[String,Double]]) extends Event
  case class AddServicesTables_Evt(linesStops:Map[String, ListBuffer[String]],servicesLines:Map[String,ListBuffer[String]] ) extends Event

  // Sharding Name
  val shardName: String = "Neighborhood"

  // outside world if he want to send message to sharding should use this message
  case class WrapperMessage(junctionID:String, cmd: Command)

  // id extrator
  val idExtractor: ShardRegion.ExtractEntityId = {
    case WrapperMessage(id, msg) => 
      val destination:Array[String]=id.split("_")
      (destination(1),msg)
  }

  // shard resolver
  val shardResolver: ShardRegion.ExtractShardId = {
    case WrapperMessage(id, msg) => 
      val destination:Array[String]=id.split("_")
      destination(0)
  }

  def props() = Props[Junction]
}