import Utils._
import akka.actor.{Props, Actor, ActorSystem, ActorRef};
import scala.collection.mutable.Queue;
import scala.collection.mutable.Map;
import scala.math._;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import scala.collection.mutable.ListBuffer
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.control.Breaks._
import org.json._

/** A Public Transport Vehicle (Mobile Entity).
 * 
 *  @constructor create a new Public Transport Vehicle (mobile entity) with a list of stops
 *  @param stops the stops in the route of the public transport vehicle.
 */

class PublicTransportVehicle(_id: String, _transportType: String, _speed:Int, val stops: ListBuffer[String], numberOfPlaces:Int, val lineID:String, val atTheEnd:String) extends MobileEntity(_id,_transportType, _speed, stops(stops.length-1)){  
    var actualPosition=0   //actual stop in the list of stops
    var nextPosition=0
    var availability=numberOfPlaces
    val passengers:ListBuffer[Person]=ListBuffer[Person]()
    
    var first=true
    var step=1
    var reverse=false
    
    if(atTheEnd.equalsIgnoreCase("reverse")){
          reverse=true
    }
    

/** Creates a string representation of the PT Vehicle.
  * 
  *  @return the string summary
  */
    override def toString: String = {
      "id: "+id+" - stops: "+stops
    }

 /** Creates a JSON object that represents the Public Transport Vehicle
  * 
  *  @return the JSON object summary
  */
  override def toJson():JSONObject = {
    val json:JSONObject=new JSONObject()
    json.put("id",id)
    json.put("pos",actualPositionVect.toJson())
    val pass:JSONArray=new JSONArray()
    
    for(p<-passengers){
      pass.put(p.id)
    }
    json.put("passengers",pass)
    json.put("transport",_transportType)
    
    return json
   }
    
 /** Called when the PT Vehicle reach a stop
  *
  *  @return the JSON object summary
  */
    def arrived(ptServices:ListBuffer[String]):ListBuffer[Person]={
      if(first){
        first=false
      }
      else{
        actualPosition=nextPosition
        if(reverse){
          if(actualPosition==0 || actualPosition==stops.size-1){
            step=(-step)
          }
        }
        else{
          if(actualPosition==stops.size-1){
            nextPosition=(-1)
          }
        }
        
      }
      nextPosition=nextPosition+step
      dest=stops(nextPosition)
      

      val exitingPassengers:ListBuffer[Person]=ListBuffer[Person]()
      for(p<-passengers){
        if(ptServices contains p.dest){
          exitingPassengers+=p
          passengers-=p
        }
      }
      availability+=exitingPassengers.length
      return exitingPassengers
    }
    
    /** Unload the vehicle in the current stop
   * 
   *  @return a list of people that exit at the current stop 
   */
    def unload(ptServices:ListBuffer[String]):ListBuffer[Person]={
      val exitingPassengers:ListBuffer[Person]=arrived(ptServices)
      return exitingPassengers
    }
    
    
    /** Vehicle loading
   * 
   *  @param the list of passengers with their destination
   */
    def load(enteringPassengers: ListBuffer[Person]){
      passengers++=enteringPassengers
      availability-=enteringPassengers.length
    }
    
  }