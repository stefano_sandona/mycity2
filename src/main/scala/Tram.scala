import scala.collection.mutable.ListBuffer

/** A Tram (PublicTransportVehicle).
 * 
 *  @constructor create a Tram (PublicTransportVehicle) 
 */

class Tram(_id: String, _speed:Int, _stops: ListBuffer[String], _lineID:String, _atTheEnd:String) extends PublicTransportVehicle(_id, "tram", _speed,_stops, 15, _lineID, _atTheEnd) { 

/** Creates a string representation of the Tram.
  * 
  *  @return the string summary
  */  
  override def toString: String = {
      "id: "+id+"  - pos: "+actualPositionVect
    }
  }