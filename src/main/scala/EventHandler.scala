import akka.actor.{Props, Actor, ActorSystem, ActorRef,ActorSelection,ActorLogging,RootActorPath};
import org.json._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import akka.cluster.singleton.{ClusterSingletonProxySettings, ClusterSingletonProxy}
import akka.cluster.ClusterEvent._
import akka.cluster._

/** An Event Handler.
 * 
 *  @constructor create a local Event Handler
 */

class EventHandler() extends Actor with ActorLogging{
  import EventHandler._
  
  //Collected events
  //Map[frameID, events]
  val events:Map[Long,JSONArray]=Map[Long,JSONArray]()
  
  val actorName=self.path.name
  
  //Proxy Actor to communicate with the CentralEventHandler Singleton
  /*val masterProxy = context.actorOf(ClusterSingletonProxy.props(
    singletonManagerPath = "/user/master",
    settings = ClusterSingletonProxySettings(context.system).withRole(None)
  ), name = "masterProxy")*/
  
  //subscribe to the CentralEventHandler
  /*masterProxy ! CentralEventHandler.Subscribe()*/
  
  //entities that will publish events
  val publishingEntities:ListBuffer[String]=ListBuffer[String]()
  
  //Senders per frame
  //Map[frameID,senders]
  val receivedEvents:Map[Long,ListBuffer[String]]=Map[Long,ListBuffer[String]]()
  
  var centralEventHandler:ActorSelection=null
  
  override def preStart() {
    context.actorSelection("/user/" + Reaper.name) ! Reaper.WatchMe(self)
    //subscribe to Cluster Events
    (Cluster(context.system)).subscribe(self,classOf[MemberUp])
  }

      
    def receive = {
    
      case MemberUp(member) =>
         //if the node is a frontend we send to him a BackendRegistration message
         if(member.hasRole("central_event_handler")){
           log.info("Central event handler detected")
           centralEventHandler=context.actorSelection(RootActorPath(member.address) / "user" / "ceh")
           centralEventHandler ! CentralEventHandler.Subscribe()
         }
    
      /*Message received from a publishing entity with the collected events of the given frame*/
      case Utils.EventMsg(frameID,json) =>
        if(!(events contains frameID)){
          events(frameID)= new JSONArray()
        }
        if(!(receivedEvents contains frameID)){
          receivedEvents(frameID)= ListBuffer[String]()
        }
        receivedEvents(frameID)+=""+sender
        val entities:JSONArray=json.getJSONArray("entities")
        if(entities.length()!=0){
          events(frameID).put(json)
        }

        if(receivedEvents(frameID).length==publishingEntities.length){
          centralEventHandler ! CentralEventHandler.Push(frameID,""+events(frameID))
          
        }
        
      /*Message received from an entity to subscribe to the publishing entities*/
      case Subscribe() =>
        publishingEntities+=(""+sender)
      
     case default =>
       log.info(s"Received an Unhandled message from ${sender} -> ${default}")
           
    }
}

object EventHandler{
  case class Subscribe()
  val name = "eventHandler"
}