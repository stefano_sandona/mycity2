import scala.math.BigDecimal.RoundingMode
import org.json._

/** A 2D vector.
 * 
 *  @constructor create a new vector with an X and Y value.
 *  @param X the vector's X coordinate
 *  @param Y the vector's Y coordinate
 */
class Vector2(var x:BigDecimal, var y:BigDecimal) extends Serializable {
  
  x=x.setScale(2, RoundingMode.FLOOR)
  y=y.setScale(2, RoundingMode.FLOOR)
  
  
/** Creates a JSON object that represents the Vector
  * 
  *  @return the JSON object summary
  */
  def toJson():JSONObject = {
    val json:JSONObject=new JSONObject()
    json.put("x",x)
    json.put("y",y)
    return json
  }
  
/** Creates a new Vector from the actual Vector to the given Vector
  * 
  *  @param a Vector
  *  @return the new Vector
  */
  def getDistanceVector(v2:Vector2):Vector2 = {
    var newX=v2.x-x
    var newY=v2.y-y
    return new Vector2(v2.x-x,v2.y-y)
  }
  
/** Compute the distance between to vectors
  * 
  *  @param a Vector
  *  @return the distance
  */
  def getDistance(v2:Vector2):BigDecimal = {
    return getDistanceVector(v2).getNorm()
  }
  
/** Creates a new Vector from the actual Vector of a certain dimension
  * 
  *  @param the length
  *  @return the new Vector
  */
  def getVectOfLenght(length:BigDecimal):Vector2 = {
    return getNormalizedVector().scaleVect(length)
  }
  
/** Reverse a Vector
  */
  def reverse():Unit = {
    x-=2*x
    y-=2*y
  }
  
/** Check if two Vectors are equals
  * 
  *  @param a Vector
  *  @return true if the two vectors are equal, false otherwise
  */
  override def equals(that: Any): Boolean =
    that match {
      case that: Vector2 => that.x==x && that.y==y
      case _ => false
   }
  
/** Clone the actual vector
  * 
  *  @return the new Vector
  */
  override def clone():Vector2 = {
    return new Vector2(x,y)
  }
  
  
/** Creates a new Vector that is the sum of the actual Vector and the given Vector
  * 
  *  @param a Vector
  *  @return the new Vector
  */
  def add(v2: Vector2):Vector2 = {
    var newX=x+v2.x
    newX=newX.setScale(2, RoundingMode.FLOOR)
    var newY=y+v2.y
    newY=newY.setScale(2, RoundingMode.FLOOR)
    return new Vector2(newX, newY)
  }
  
  def getNorm():BigDecimal = {
    var norm=Math.sqrt(Math.pow(x.toDouble,2)+Math.pow(y.toDouble,2))
    val strNorm=norm.toString()
    var bdnorm=BigDecimal.double2bigDecimal(norm)
    bdnorm=bdnorm.setScale(2, RoundingMode.FLOOR)
    return bdnorm
  }
  
/** Creates a new Vector that is the actual Vector normalized
  * 
  *  @return the new Vector
  */
  def getNormalizedVector():Vector2 = {
    val norm=getNorm()
    var newX=x/norm
    var newY=y/norm
    return new Vector2(newX,newY)
  }
  
/** Scale the actual Vector
  * 
  *  @param the scale
  *  @return the new Vector
  */
  def scaleVect(scale: BigDecimal): Vector2 = {
    var newX=x*scale
    var newY=y*scale
    return new Vector2(newX,newY)
  }
  
/** Creates a string representation of the Vector.
  * 
  *  @return the string summary
  */
  override def toString: String = {
    "("+x+","+y+")"
  }
}